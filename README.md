This application consist of several features (up to 20/3/2019)
- Read PDF file text
- Enable cutomized paging
- Search and replace key word
- Customized font styling

*Note : begin this application from PDFwithTextOnlyV2.


To change the startup page,
- Step 1 : Open App.xaml file from the Solution Explorer.
- Step 2 : Within Application tag, look for "StartupUri".
- Step 3 : Rename the file name to the file that you wish to start with.
 

To view error message written by service, get the application from link below:
https://gitlab.com/loochuckie21/eventlog.git


To get the service used in this application, please get from link below:
https://gitlab.com/loochuckie21/learnsv.git
