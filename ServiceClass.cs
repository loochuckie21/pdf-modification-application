﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;

namespace LearnPDF
{
    class ServiceClass
    {
        private string serviceName;
        private readonly string machinename = Environment.MachineName;
        private ServiceController service;
        public ServiceClass()
        {

        }
        public ServiceClass(string ServiceName)
        {
            this.ServiceName = ServiceName;
            service= new ServiceController(serviceName,machinename);
        }

        public void StartService(int timeoutMilliseconds)
        {
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
            catch (System.ServiceProcess.TimeoutException ex)
            {
                throw new System.ServiceProcess.TimeoutException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void StartService(int timeoutMilliseconds, string[] args)
        {
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Start(args);
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
            catch (System.ServiceProcess.TimeoutException ex)
            {
                throw new System.ServiceProcess.TimeoutException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void StopService(int timeoutMilliseconds)
        {
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
            }
            catch (System.ServiceProcess.TimeoutException ex)
            {
                throw new System.ServiceProcess.TimeoutException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void RestartService(int timeoutMilliseconds)
        {
            try
            {
                int millisec1 = Environment.TickCount;
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);

                // count the rest of the timeout
                int millisec2 = Environment.TickCount;
                timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (millisec2 - millisec1));

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
            catch (System.ServiceProcess.TimeoutException ex)
            {
                throw new System.ServiceProcess.TimeoutException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool IsServiceInstalled()
        {
            // get list of Windows services
            ServiceController[] services = ServiceController.GetServices();

            // try to find service name
            foreach (ServiceController service in services)
            {
                if (service.ServiceName.Contains(serviceName))
                    return true;
            }
            return false;
        }

        public ServiceControllerStatus GetServiceStatus()
        {
            return service.Status;
        }

        public List<string> GetServicesList()
        {
            List<string> rs = new List<string>();

            ServiceController[] services = ServiceController.GetServices();
            foreach (var sv in services)
            {
                rs.Add(sv.ServiceName);
            }


            return rs;
        }

        public string ServiceName { get => serviceName; set => serviceName = value; }
    }
}
