﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace LearnPDF
{
    /// <summary>
    /// Interaction logic for SearchDialog.xaml
    /// </summary>
    public partial class SearchDialog : Window
    {
        private List<ContentClass> content = new List<ContentClass>();
        public SearchDialog()
        {
            InitializeComponent();
            if (content.Count == 0 || content == null)
            {
                lblResult.Text = "No information to search.";
                txtSearch.IsEnabled = false;
                btnSearch.IsEnabled = false;
            }
        }
        public SearchDialog(List<ContentClass> data)
        {
            InitializeComponent();
            if (data.Count == 0 || data == null)
            {
                lblResult.Text = "No information to search.";
                txtSearch.IsEnabled = false;
                btnSearch.IsEnabled = false;
            }
            else
            {
                lblResult.Text = "Enter keyword first.";
                txtSearch.Focus();
                /*for (int pg = 0; pg < data.Count; pg++)
                {
                    for (int ln = 0; ln < data[pg].Length; ln++)
                    {
                        content.Add(new ContentClass(pg, ln, data.ElementAt(pg)[ln]));
                    }
                }*/
                content = data;
            }
        }

        public string SelectedItem
        {
            get
            {
                return ((ContentClass)lvSearchResult.SelectedValue).Words; ;
            }
        }

        public string SearchKey
        {
            get { return txtSearch.Text; }
        }

        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            lvSearchResult.ItemsSource = null;
            string word = txtSearch.Text;
            List<ContentClass> result = new List<ContentClass>();
            for (int i = 0; i < content.Count; i++)
            {
                if (content[i].Words.Contains(word))
                {
                    result.Add(new ContentClass(content[i].Page, content[i].Line, content[i].Words));
                }

                foreach (Match m in Regex.Matches(content[i].Words, word)){
                    content[i].addKeyIndex(m.Index);
                }
            }

            if (result.Count <= 0)
                lvSearchResult.Items.Add("No result");
            else
            {
                if (lvSearchResult.Items.Count >= 1)
                    lvSearchResult.Items.Clear();
                lvSearchResult.ItemsSource = result;
                lblResult.Text = string.Format("{0} item(s) found", result.Count);
            }
        }

        private void TxtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
                BtnSearch_Click(sender, e);

            if (e.Key == Key.Escape)
                txtSearch.Text = "";
            txtSearch.Focus();
        }

        private void LvSearchResult_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (lvSearchResult.SelectedIndex != -1)
            {
                DialogResult = true;
                e.Handled = true;
            }
        }

            private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            e.Handled = true;
        }

    }
}
