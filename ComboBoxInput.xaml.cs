﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;

namespace LearnPDF
{
    /// <summary>
    /// ComboboxInput is only interact with PDFwithTextOnlyV2
    /// </summary>
    public partial class ComboBoxInput : Window
    {
        private bool yrEdit = false;
        private bool mnEdit = false;
        private bool dyEdit = false;
        private int lines;
        public ComboBoxInput()
        {
            InitializeComponent();

            cbxYear.Items.Clear();
            string sqlQ = "SELECT DISTINCT DATEPART(year, m.StoreDate) as yr FROM dbo.Menu as m ORDER BY yr";
            string conn = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection sqlcon = new SqlConnection(conn))
            {
                sqlcon.Open();
                using (SqlCommand cmd = new SqlCommand(sqlQ, sqlcon))
                {
                    using (SqlDataReader sqlRd = cmd.ExecuteReader())
                    {
                        while (sqlRd.Read())
                        {
                            cbxYear.Items.Add(sqlRd["yr"]);
                        }
                    }
                }
                sqlcon.Close();
            }
            if (cbxYear.Items.Count > 0)
            {
                yrEdit = true;
                cbxYear.SelectedIndex = 0;
            }
            else
            {
                MessageBox.Show("No record found.");
                DialogResult = false;
            }
        }

        public DateTime? GetSelectedDate
        {
            get
            {
                string inString = cbxDay.SelectedItem.ToString();
                inString += @"/" + cbxMonth.SelectedItem.ToString();
                inString += @"/" + cbxYear.SelectedItem.ToString();
                inString += @" " + cbxTime.SelectedItem.ToString();
                DateTime dateValue;
                if (DateTime.TryParse(inString, out dateValue))
                    return dateValue;
                else
                    return null;
            }
        }

        public int GetLines { get { return lines; } }

        public string GetFormType
        {
            get
            {
                if (rbText.IsChecked == true)
                    return "Text";
                else
                    return "Menu";
            }
        }

        private void CbxYear_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (yrEdit)
            {
                mnEdit = false;
                cbxMonth.Items.Clear();
                cbxMonth.IsEnabled = true;
                string sqlQ = "SELECT DISTINCT DATEPART(month, m.StoreDate) as mnth FROM dbo.Menu as m where year(m.storedate)= @yr ORDER BY mnth";
                string conn = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection sqlcon = new SqlConnection(conn))
                {
                    sqlcon.Open();
                    using (SqlCommand cmd = new SqlCommand(sqlQ, sqlcon))
                    {
                        cmd.Parameters.AddWithValue("yr", cbxYear.SelectedItem.ToString());
                        using (SqlDataReader sqlRd = cmd.ExecuteReader())
                        {
                            while (sqlRd.Read())
                            {
                                cbxMonth.Items.Add(sqlRd["mnth"]);
                            }
                        }
                    }
                    sqlcon.Close();
                }
                mnEdit = true;
                cbxMonth.SelectedIndex = 0;
            }
        }

        private void CbxMonth_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (mnEdit)
            {
                dyEdit = false;
                cbxDay.Items.Clear();
                cbxDay.IsEnabled = true;
                //continue here
                string sqlQ = "SELECT DISTINCT day(m.StoreDate) as dy FROM dbo.Menu as m where year(m.StoreDate) = @yr and month(m.StoreDate)=@mnth ORDER BY dy";
                string conn = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection sqlcon = new SqlConnection(conn))
                {
                    sqlcon.Open();
                    using (SqlCommand cmd = new SqlCommand(sqlQ, sqlcon))
                    {
                        cmd.Parameters.AddWithValue("mnth", cbxMonth.SelectedItem.ToString());
                        cmd.Parameters.AddWithValue("yr", cbxYear.SelectedItem.ToString());
                        using (SqlDataReader sqlRd = cmd.ExecuteReader())
                        {
                            while (sqlRd.Read())
                            {
                                cbxDay.Items.Add(sqlRd["dy"]);
                            }
                        }
                    }
                    sqlcon.Close();
                }
                dyEdit = true;
                cbxDay.SelectedIndex = 0;
            }

        }

        private void CbxDay_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dyEdit)
            {
                cbxTime.Items.Clear();
                string sqlQ = "SELECT DISTINCT cast(m.StoreDate as time) as tm FROM dbo.Menu as m where cast(m.StoreDate as date) = cast(@dt as date) ORDER BY tm";
                string conn = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                DateTime dt = new DateTime(int.Parse(cbxYear.SelectedItem.ToString()), int.Parse(cbxMonth.SelectedItem.ToString()), int.Parse(cbxDay.SelectedItem.ToString()));
                using (SqlConnection sqlcon = new SqlConnection(conn))
                {
                    sqlcon.Open();
                    using (SqlCommand cmd = new SqlCommand(sqlQ, sqlcon))
                    {
                        cmd.Parameters.AddWithValue("dt", dt);
                        using (SqlDataReader sqlRd = cmd.ExecuteReader())
                        {
                            while (sqlRd.Read())
                            {
                                cbxTime.Items.Add(sqlRd["tm"]);
                            }
                        }
                    }
                    sqlcon.Close();
                }
                cbxTime.IsEnabled = true;
                cbxTime.SelectedIndex = 0;
            }
        }

        private void BtnOK_Click(object sender, RoutedEventArgs e)
        {
            if (rbText.IsChecked == true)
            {
                InputDialog idLines = new InputDialog("Enter number of lines for each page", "Lines", DataType.aNumber);
                bool? yn = idLines.ShowDialog();
                if (yn == true)
                {
                    lines = int.Parse(idLines.inputText);
                }
                else
                    return;
            }
            DialogResult = true;
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

    }
}
