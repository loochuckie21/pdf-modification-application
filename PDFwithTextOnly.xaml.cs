﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LearnPDF
{
    /// <summary>
    /// Interaction logic for PDFwithTextOnly.xaml
    /// </summary>
    public partial class PDFwithTextOnly : Window
    {
        private string filename;
        private List<string[]> content = new List<string[]>();
        private List<ContentClass> pdfContent = new List<ContentClass>();
        private string search;
        private string searchkey;
        public PDFwithTextOnly()
        {
            InitializeComponent();
        }

        private void BtnBrowse_Click(object sender, RoutedEventArgs e)
        {
            GC.Collect();
            Microsoft.Win32.FileDialog fdpdf = new Microsoft.Win32.OpenFileDialog();
            fdpdf.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
            fdpdf.Filter = "PDF File (*.pdf;)|*.pdf;";
            bool? yn = fdpdf.ShowDialog();
            if (yn == true)
                filename = fdpdf.FileName;
            else
                return;
            PdfReader reader = new PdfReader(filename);
            content.Clear();
            try
            {
                for (int pg = 0; pg < reader.NumberOfPages; pg++)
                {
                    string text = PdfTextExtractor.GetTextFromPage(reader, pg + 1);
                    string[] val = text.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
                    for (int line = 0; line < val.Length; line++)
                    {
                        val[line] = Regex.Replace(val[line], @"\s\s+", @" ");        //replace all large whitespace to empty
                        val[line] = val[line].Trim();
                        if (!string.IsNullOrWhiteSpace(val[line]) || !string.IsNullOrEmpty(val[line]))
                            pdfContent.Add(new ContentClass(pg, line, val[line]));
                    }
                    val = val.Where(str => !string.IsNullOrWhiteSpace(str)).ToArray();          //remove empty cell
                    val = val.Where(str => !string.IsNullOrEmpty(str)).ToArray();          //remove empty cell
                    content.Add(val);
                }
                writeContent();
                reader.Close();
            }
            catch (IOException ioExp)
            {
                MessageBox.Show(ioExp.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            Document doc = new Document(PageSize.LETTER, 30, 30, 30, 30);
            PdfWriter write = PdfWriter.GetInstance(doc, new FileStream(filename, FileMode.Create));

            doc.Open();
            //write text into document
            for (int pageNum = 0; pageNum < content.Count; pageNum++)
            {
                string[] strArr = content.ElementAt(pageNum);
                string writeData = "";
                for (int ind = 0; ind < strArr.Length; ind++)
                {
                    writeData += strArr[ind] + "\n";
                }

                iTextSharp.text.Paragraph pr = new iTextSharp.text.Paragraph(writeData);
                doc.Add(pr);
                if (pageNum + 1 != content.Count)
                {
                    doc.NewPage();
                }
            }



            doc.Close();

            System.Windows.MessageBox.Show("Save complete");

            //            filename = null;
            GC.Collect();
            GC.Collect();

        }

        private void BtnSaveAs_Click(object sender, RoutedEventArgs e)
        {

        }

        private void TxtPDFval_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        private void BtnFind_Click(object sender, RoutedEventArgs e)
        {
            SearchDialog SD = new SearchDialog(pdfContent);
            SD.ShowDialog();
            if (SD.DialogResult == true)
            {
                search = SD.SelectedItem.ToString();
                searchkey = SD.SearchKey;
                MessageBox.Show(SD.SelectedItem.ToString());
            }
            else
                return;
            fld.Blocks.Clear();
            //for (int pageNum = 0; pageNum < content.Count; pageNum++)
            //{
            //    string[] strArr = content.ElementAt(pageNum);
            //    for (int ind = 0; ind < strArr.Length; ind++)
            //    {
            //        System.Windows.Documents.Paragraph prg = new System.Windows.Documents.Paragraph();
            //        Run r1 = new Run(strArr[ind]);
            //        if (strArr[ind].Contains(search))
            //            r1.Foreground = Brushes.Blue;
            //        prg.Inlines.Add(r1);


            //        fld.Blocks.Add(prg);
            //        if (ind + 1 == strArr.Length && pageNum + 1 != content.Count)
            //        {
            //            fld.Blocks.Add(new System.Windows.Documents.Paragraph(new Run("")) { BreakPageBefore = true });
            //        }
            //    }
            //}

            foreach(ContentClass cc in pdfContent)
            {
                System.Windows.Documents.Paragraph prg = new System.Windows.Documents.Paragraph();
                Run r1 = new Run(cc.Words);

                if (cc.Words.Contains(search))
                    r1.Foreground = Brushes.Blue;
                prg.Inlines.Add(r1);


                fld.Blocks.Add(prg);
            }


            fld.LineHeight = 5;


        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            //EditTextDialog EDT = new EditTextDialog(content, search);
            EditTextDialog EDT1 = new EditTextDialog(pdfContent, search,searchkey);
            bool? rs = EDT1.ShowDialog();
            if (rs == true)
            { }// content = EDT.Contents;
            else
                return;


            writeContent();
        }

        private void writeContent()
        {
            fld.Blocks.Clear();
            //for (int pageNum = 0; pageNum < content.Count; pageNum++)
            //{
            //    string[] strArr = content.ElementAt(pageNum);
            //    for (int ind = 0; ind < strArr.Length; ind++)
            //    {
            //        System.Windows.Documents.Paragraph prg = new System.Windows.Documents.Paragraph();
            //        prg.Inlines.Add(new Run(strArr[ind]));
            //        fld.Blocks.Add(prg);
            //        if (ind + 1 == strArr.Length && pageNum + 1 != content.Count)
            //        {
            //            fld.Blocks.Add(new System.Windows.Documents.Paragraph(new Run("")) { BreakPageBefore = true });
            //        }
            //    }
            //}

            int pg = 0;
            foreach (ContentClass cc in pdfContent)
            {
                System.Windows.Documents.Paragraph prg = new System.Windows.Documents.Paragraph();
                if (pg != cc.Page)
                {
                    fld.Blocks.Add(new System.Windows.Documents.Paragraph(new Run("")) { BreakPageBefore = true });
                    pg++;
                }
                prg.Inlines.Add(new Run(cc.Words));
                fld.Blocks.Add(prg);
            }
            fld.LineHeight = 5;
        }
    }
}
