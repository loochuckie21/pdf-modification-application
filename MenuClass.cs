﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnPDF
{
    class MenuClass : INotifyPropertyChanged
    {
        #region variables and accessors
        private string shopName;
        private string shopAddress;
        private ObservableCollection<MenuCategory> menus = new ObservableCollection<MenuCategory>();
        private string shopContact;
        private System.Drawing.Font defaultFont;

        public event PropertyChangedEventHandler PropertyChanged;

        public string ShopName { get => shopName; set => shopName = value; }
        public string ShopAddress { get => shopAddress; set => shopAddress = value; }
        public string ShopContact { get => shopContact; set => shopContact = value; }
        internal ObservableCollection<MenuCategory> Menus { get => menus; set => menus = value; }
        public System.Drawing.Font DefaultFont { get => defaultFont; set => defaultFont = value; }
        #endregion

        #region constructors
        public MenuClass()
        {
        }

        public MenuClass(string shopName)
        {
            ShopName = shopName;
        }

        public MenuClass(string shopName, MenuCategory menu) : this(shopName)
        {
            Menus.Add(menu);
        }

        public MenuClass(string shopName, string shopAddress, string shopContact) : this(shopName)
        {
            ShopAddress = shopAddress;
            ShopContact = shopContact;
        }

        public MenuClass(string shopName, string shopAddress, string shopContact, ObservableCollection<MenuCategory> menus)
            : this(shopName, shopAddress, shopContact)
        {
            Menus = menus;
        }
        #endregion

        #region methods
        /// <summary>
        /// Add new category into the list, exception thrown if existed.
        /// </summary>
        /// <param name="CategoryName">New category name</param>
        public void AddCategory(string CategoryName)
        {
            bool found = false;
            foreach (var category in Menus)
                if (category.Category.Equals(CategoryName))
                    found = true;
            if (!found)
                Menus.Add(new MenuCategory(CategoryName));
            else
                throw new Exception(string.Format("\"{0}\" is already existed.", CategoryName));
        }


        public void AddSubCategory(int CategoryIndex, string subCategoryName)
        {
            try
            {
                menus[CategoryIndex].AddSubCategory(subCategoryName);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Add new Menu Item. 
        /// Null Exception thrown when Category index or Sub-Category index is not found and
        /// Exception thrown when item is already exist.
        /// </summary>
        /// <param name="CategoryIndex">The index of category</param>
        /// <param name="SubCategoryIndex">The index of sub-category</param>
        /// <param name="ItemName">The name of new item</param>
        /// <param name="price">The price of new item</param>
        public void AddMenuItem(int CategoryIndex, int SubCategoryIndex, string ItemName, double price)
        {
            try
            {
                menus[CategoryIndex].Subcategories[SubCategoryIndex].AddItem(ItemName, price);
            }
            catch (NullReferenceException)
            {
                throw new NullReferenceException("Category not found.");
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Unable to add item. {0}", ex.Message));
            }
        }

        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public bool ContainsCategory(string CateName)
        {
            foreach (var m in Menus)
                if (m.Category.Equals(CateName))
                    return true;
            return false;
        }
        #endregion
    }//end Menu class

    partial class MenuCategory
    {
        #region variables and accessors
        private string category;
        private ObservableCollection<MenuSubCategory> subcategories = new ObservableCollection<MenuSubCategory>();
        private static System.Drawing.Font defaultFont;

        public string Category { get => category; set => category = value; }
        internal ObservableCollection<MenuSubCategory> Subcategories { get => subcategories; set => subcategories = value; }
        public System.Drawing.Font DefaultFont { get => defaultFont; set => defaultFont = value; }
        #endregion

        #region constructors
        public MenuCategory()
        {
        }

        public MenuCategory(string category)
        {
            Category = category;
        }

        public MenuCategory(string category, MenuSubCategory subcategory) : this(category)
        {
            Subcategories.Add(subcategory);
        }

        public MenuCategory(string category, ObservableCollection<MenuSubCategory> subcategories) : this(category)
        {
            Subcategories = subcategories;
        }
        #endregion

        #region methods
        /// <summary>
        /// Add a new sub-category into category, exception thrown when sub-category existed.
        /// </summary>
        /// <param name="subCategory">New sub-category name.</param>
        public void AddSubCategory(string subCategory)
        {
            bool found = false;
            foreach (var sub in subcategories)
                if (sub.SubCategory.Equals(subcategories))
                    found = true;
            if (!found)
                subcategories.Add(new MenuSubCategory(subCategory));
            else
                throw new Exception(string.Format("\"{0}\" is already exist in this category.", subCategory));
        }

        public bool ContainsSubCate(string SubCate)
        {
            foreach (var s in subcategories)
                if (s.SubCategory.Equals(SubCate))
                    return true;
            return false;
        }
        #endregion
    }

    partial class MenuSubCategory
    {
        #region variables and accessors
        private string subcategory;
        private ObservableCollection<ItemDetail> items = new ObservableCollection<ItemDetail>();
        private static System.Drawing.Font defaultFont;

        public string SubCategory { get => subcategory; set => subcategory = value; }
        internal ObservableCollection<ItemDetail> Items { get => items; set => items = value; }
        public System.Drawing.Font DefaultFont { get => defaultFont; set => defaultFont = value; }
        #endregion

        #region constructor
        public MenuSubCategory()
        {
        }

        public MenuSubCategory(string subcategory)
        {
            SubCategory = subcategory;
        }

        public MenuSubCategory(string subcategory, ItemDetail item) : this(subcategory)
        {
            AddItem(item.Item, item.Price);
        }

        public MenuSubCategory(string subcategory, ObservableCollection<ItemDetail> items) : this(subcategory)
        {
            Items = items;
        }

        #endregion

        #region methods
        /// <summary>
        /// Add new item into the list
        /// </summary>
        /// <param name="item">Item name</param>
        /// <param name="price">Item price</param>
        public void AddItem(string item, double price)
        {
            bool found = false;
            foreach (var name in Items)
            {
                if (name.Item.Equals(item))
                    found = true;
            }

            if (!found)
            {
                Items.Add(new ItemDetail(item, price));
            }
            else
            {
                throw new Exception(string.Format("\"{0}\" already existed in this sub-category.", item));
            }
        }

        /// <summary>
        /// Rename the item
        /// </summary>
        /// <param name="oldItem">The item to be replaced</param>
        /// <param name="newItem">The new item name</param>
        /// <returns>true if replaced, vice-versa</returns>
        public bool RenameItem(string oldItem, string newItem)
        {
            bool found = false;
            for (int i = 0; i < Items.Count && !found; i++)
            {
                if (Items[i].Equals(oldItem))
                {
                    Items[i].Item = newItem;
                    found = true;
                }
            }
            return found;
        }

        /// <summary>
        /// Generate menu items in sub-category in pdf table form
        /// </summary>
        /// <param name="Currency">The money currency</param>
        /// <returns></returns>
        public PdfPTable getSubCatePDFTable(string Currency)
        {
            PdfPTable table = new PdfPTable(2);//2 columns, item and price
            iTextSharp.text.Font subcatefont = new iTextSharp.text.Font();
            if (DefaultFont != null)
            {
                subcatefont = FontFactory.GetFont(DefaultFont.FontFamily.Name, DefaultFont.Size);
                if (DefaultFont.Italic)
                    subcatefont.SetStyle(iTextSharp.text.Font.ITALIC);
                if (DefaultFont.Bold)
                    subcatefont.SetStyle(iTextSharp.text.Font.BOLD);
            }

            PdfPCell tableHeading = new PdfPCell(new Phrase(SubCategory, subcatefont));//can insert styling here
            tableHeading.Colspan = 2;
            tableHeading.PaddingTop = 20f;
            PdfPCell headercell1 = new PdfPCell(new Phrase("Items", subcatefont));
            PdfPCell headercell2 = new PdfPCell(new Phrase(Currency, subcatefont));
            table.HeaderRows = 1;
            table.AddCell(tableHeading);
            table.AddCell(headercell1);
            table.AddCell(headercell2);


            
            foreach (var item in Items)
            {
                table.AddCell(new Phrase(item.GetItemPhrase()));
                table.AddCell(new Phrase(item.GetPricePhrase()));
            }
            table.TableEvent = new SplitTableWatcher(); // for detect if the table is seperated to another page
            return table;
        }

        public bool ContainsItem(string item)
        {
            foreach (var i in Items)
                if (i.Item.Equals(item))
                    return true;
            return false;
        }
        #endregion
    }

    partial class ItemDetail
    {
        #region variables and accessors
        private string items;
        private double price;
        private static System.Drawing.Font defaultFont;

        public double Price { get => price; set => price = value; }
        public string Item { get => items; set => items = value; }
        public System.Drawing.Font DefaultFont { get => defaultFont; set => defaultFont = value; }
        #endregion

        #region constructors
        public ItemDetail(string items, double price)
        {
            Price = price;
            Item = items;
        }
        #endregion

        public Phrase GetItemPhrase()
        {
            Phrase itemP=null;
            iTextSharp.text.Font itemfont = new iTextSharp.text.Font();
            if (DefaultFont != null)
            {
                itemfont = FontFactory.GetFont(DefaultFont.FontFamily.Name, DefaultFont.Size);
                if (DefaultFont.Italic)
                    itemfont.SetStyle(iTextSharp.text.Font.ITALIC);
                if (DefaultFont.Bold)
                    itemfont.SetStyle(iTextSharp.text.Font.BOLD);
            }
            itemP = new Phrase(Item, itemfont);
            return itemP;
        }
        public Phrase GetPricePhrase()
        {
            Phrase itemP=null;
            iTextSharp.text.Font itemfont = new iTextSharp.text.Font();
            if (DefaultFont != null)
            {
                itemfont = FontFactory.GetFont(DefaultFont.FontFamily.Name, DefaultFont.Size);
                if (DefaultFont.Italic)
                    itemfont.SetStyle(iTextSharp.text.Font.ITALIC);
                if (DefaultFont.Bold)
                    itemfont.SetStyle(iTextSharp.text.Font.BOLD);
            }
            itemP = new Phrase(string.Format("{0:0.00}",Price), itemfont);
            return itemP;
        }
    }

}
