﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace LearnPDF
{
    public class ContentClass
    {
        private int page;
        private int line;
        private string words;
        private Dictionary<int, System.Drawing.Font> style = new Dictionary<int, System.Drawing.Font>();
        private Dictionary<int, int> textrange = new Dictionary<int, int>();
        private static System.Drawing.Font defaultFont;


        public List<int> KeyWordIndexes = new List<int>();


        #region constructors
        public ContentClass() { }

        public ContentClass(int page, int line, string words)
        {
            this.Page = page;
            this.Line = line;
            this.Words = words;
        }

        public ContentClass(int page, int line, string words, List<int> keyIndexes) : this(page, line, words)
        {
            KeyIndexes = keyIndexes;
        }

        public ContentClass(int page, int line, string words, Dictionary<int, System.Drawing.Font> style,
            Dictionary<int, int> textrange, List<int> keyWordIndexes) : this(page, line, words)
        {
            this.style = style;
            this.textrange = textrange;
            KeyWordIndexes = keyWordIndexes;
        }

        public ContentClass(int page, int line, string words, List<int> keyIndexes, Dictionary<int, System.Drawing.Font> style,
            Dictionary<int, int> textrange) : this(page, line, words, keyIndexes)
        {
            Style = style;
            Textrange = textrange;
        }
        #endregion

        #region getter setter
        public int Line { get => line; set => line = value; }
        public int Page { get => page; set => page = value; }
        public string Words { get => words; set => words = value; }
        public List<int> KeyIndexes { get => KeyWordIndexes; set => KeyIndexes = value; }
        public Dictionary<int, System.Drawing.Font> Style { get => style; set => style = value; }
        public Dictionary<int, int> Textrange { get => textrange; set => textrange = value; }
        public System.Drawing.Font DefaultFont { get => defaultFont; set => defaultFont = value; }
        #endregion

        public void AddTextStyle(int StartIndex, int NumOfWords, System.Drawing.Font st)
        {
            if (textrange.ContainsKey(StartIndex))
            {
                if (NumOfWords > textrange[StartIndex]) // if the range is larger than original range
                {
                    textrange[StartIndex] = NumOfWords;
                    style[StartIndex] = st;
                    foreach (var key in textrange.Keys.ToList())
                    {
                        int lastind = textrange[key] + key;

                        //remove the range between the range of new added range, and the style
                        if (StartIndex <= key && lastind < StartIndex + NumOfWords)
                        {
                            textrange.Remove(key);
                            style.Remove(key);
                        }
                    }
                }
                else // cut the original range, make it new range
                {
                    int NewStartIndex = StartIndex + NumOfWords;
                    int NewNumOfWord = textrange[StartIndex] - NumOfWords;
                    AddTextStyle(NewStartIndex, NewNumOfWord, style[StartIndex]);   //new cut range
                    style[StartIndex] = st;                                         //replace original style
                    textrange[StartIndex] = NumOfWords;                             //replace original range
                }
                /* Sampling : 0123456789,
                 * Original(textrange)  start = 0, word = 7, style size 20 (0123456)
                   New(input)           start = 0, word = 5, style size 50 (01234)

                   Original(Replaced)   start = 0, word = 5, style size 50 (01234)
                   new(cut)             start = 5, word = 2, style size 20 (56)
                */
            }
            else
            {
                textrange.Add(StartIndex, NumOfWords);
                style.Add(StartIndex, st);
            }
            textrange = textrange.Keys.OrderBy(k => k).ToDictionary(k => k, k => textrange[k]); //linq sort dictionary
            style = style.Keys.OrderBy(k => k).ToDictionary(k => k, k => style[k]); //linq sort dictionary
            int lastInd = -1;
            int prevKey = -1;
            foreach (var key in textrange.Keys.ToList())        //check whether the range crashed to each other
            {
                if (lastInd > key)
                {
                    int nextLastIndex = textrange[key] + key;
                    if (nextLastIndex < lastInd)
                    {
                        //Sample : previous : 01234567899,  next : 4567
                        //Sample : previous : 0123,         next : 4567,    new : 899
                        textrange[prevKey] -= (lastInd - key);
                        AddTextStyle(nextLastIndex, lastInd - nextLastIndex, style[prevKey]);
                    }
                    else
                    {
                        //reduce the range if previous last word index is more than next begin index
                        //eg : prev last index : 9, next start index: 5, prevlastind-nextstartind = 4 (reduce word amount for prev)
                        //Sample : previous 0123456789  next 56789
                        //Result : previous 01234       next 56789
                        textrange[prevKey] -= (lastInd - key);
                    }
                }

                lastInd = key + textrange[key];
                prevKey = key;
            }
        }

        public void addKeyIndex(int ind)
        {
            KeyIndexes.Add(ind);
        }

        private string getKey()
        {
            string rs = "";
            foreach (int a in KeyIndexes)
                rs += string.Format("{0} ", a);

            return rs;
        }

        public override string ToString()
        {
            return string.Format("Page {0} Line {1:000}: {2} ||Key index: {3}", page, line, words, getKey());
        }

        /// <summary>
        /// Generate a single iTextSharp.Text.Paragraph with font styling. Highly associate with "textrange" field
        /// </summary>
        /// <returns>Styled text in iTextSharp.Text.Paragraph</returns>
        public Paragraph GetPdfParagraph()
        {
            Paragraph pr = new Paragraph();
            string temp = words;
            string remainWord = words;
            //if (textrange.Count > 0)
            // {
            int tr = 0;//for textrange index
            while (remainWord.Length > 0)
            {
                string ChunkContent;
                if (tr < textrange.Count)
                {
                    if (textrange.ElementAt(tr).Key == temp.IndexOf(remainWord)) // add style to the chunk
                    {
                        ChunkContent = temp.Substring(textrange.ElementAt(tr).Key, textrange.ElementAt(tr).Value);
                        remainWord = remainWord.Remove(0, ChunkContent.Length);
                        Chunk ch = new Chunk(ChunkContent);
                        ch.Font = FontFactory.GetFont(style.ElementAt(tr).Value.FontFamily.Name, style.ElementAt(tr).Value.Size);
                        if (style.ElementAt(tr).Value.Italic)
                            ch.Font.SetStyle(iTextSharp.text.Font.ITALIC);
                        if (style.ElementAt(tr).Value.Bold)
                            ch.Font.SetStyle(iTextSharp.text.Font.BOLD);

                        pr.Add(ch);

                        tr++;
                    }
                    else // no style
                    {

                        int sub = temp.Substring(0, textrange.ElementAt(tr).Key).Length - (temp.Length - remainWord.Length);
                        int num = remainWord.Length == temp.Length ? textrange.ElementAt(tr).Key : sub;
                        ChunkContent = temp.Substring(temp.IndexOf(remainWord), num);
                        remainWord = remainWord.Remove(0, ChunkContent.Length);
                        Chunk ch = new Chunk(ChunkContent);
                        if (DefaultFont != null)
                        {
                            ch.Font = FontFactory.GetFont(DefaultFont.FontFamily.Name, DefaultFont.Size);
                            if (DefaultFont.Italic)
                                ch.Font.SetStyle(iTextSharp.text.Font.ITALIC);
                            if (DefaultFont.Bold)
                                ch.Font.SetStyle(iTextSharp.text.Font.BOLD);
                        }
                        pr.Add(ch);
                    }
                }
                else//last
                {
                    ChunkContent = temp.Substring(temp.IndexOf(remainWord));
                    remainWord = remainWord.Remove(0, ChunkContent.Length);
                    Chunk ch = new Chunk(ChunkContent);
                    if (DefaultFont != null)
                    {
                        ch.Font = FontFactory.GetFont(DefaultFont.FontFamily.Name, DefaultFont.Size);
                        if (DefaultFont.Italic)
                            ch.Font.SetStyle(iTextSharp.text.Font.ITALIC);
                        if (DefaultFont.Bold)
                            ch.Font.SetStyle(iTextSharp.text.Font.BOLD);
                    }
                    pr.Add(ch);
                }

                //   }
            }
            return pr;
        }
    }
}
