﻿
using System.Windows;
using System.Windows.Input;

namespace LearnPDF
{
    public enum DataType
    {
        aString,
        aNumber,
        aDouble
    }
    /// <summary>
    /// Interaction logic for InputDialog.xaml
    /// </summary>
    public partial class InputDialog : Window
    {
        private DataType dt=DataType.aString;
        public InputDialog()
        {
            InitializeComponent();
            txtHeader.Focus();
            txtHeader.SelectAll();
        }

        public InputDialog(string command)
        {
            InitializeComponent();
            lblcommand.Text = command;
            txtHeader.Focus();
            txtHeader.SelectAll();
        }
        public InputDialog(string command, string title) : this(command)
        {
            this.Title = title;
        }
        public InputDialog(string command, string title,string defaultInput) : this(command,title)
        {
            txtHeader.Text = defaultInput;
        }

        public InputDialog(string command, string title, DataType dt) : this(command, title)
        {
            this.dt = dt;
        }

        public string inputText
        {
            get { return txtHeader.Text; }
            set { txtHeader.Text = value; }
        }

        private void Btn_OK_Click(object sender, RoutedEventArgs e)
        {
            switch (dt)
            {
                case DataType.aDouble: { try { double.Parse(txtHeader.Text); } catch { MessageBox.Show("Please enter number with decimals only."); return; } break; }
                case DataType.aNumber: { try { int.Parse(txtHeader.Text); } catch { MessageBox.Show("Please enter numbers only."); return; } break; }
                default: { break; }
            }

            DialogResult = true;
            e.Handled = true;
        }

        private void Btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            e.Handled = true;
        }

        private void TxtHeader_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
                Btn_OK_Click(sender, e);
            if (e.Key == Key.Escape)
                Btn_Cancel_Click(sender, e);
        }
    }
}
