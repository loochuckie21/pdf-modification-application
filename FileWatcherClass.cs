﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace LearnPDF
{
    enum Action
    {
        Create,
        Change,
        Rename,
        Delete,
        Error
    }

    enum WatchStatus
    {
        Started,
        Stopped,
        NoStatus
    }

    class FileWatcherClass
    {
        private string folder;
        private const string loc = @"\log";
        private const string file = @"logWatcher.txt";
        private bool logToDatabase = false;
        private WatchStatus status=WatchStatus.NoStatus;

        FileSystemWatcher FSW = new FileSystemWatcher();

        public string Folder { get => folder; set => folder = value; }
        public string LogFilePath { get => Folder + loc + @"\" + file; }
        public bool LogToDataBase { get => logToDatabase; set => logToDatabase = value; }
        internal WatchStatus Status { get => status; set => status = value; }


        /// <summary>
        /// Set the watcher file default to C Drive
        /// </summary>
        public FileWatcherClass()
        {
            Folder = @"C:\";
            CreateFile();
        }

        /// <summary>
        /// Set the watcher to the folder by the file path given
        /// </summary>
        /// <param name="FilePath"></param>
        public FileWatcherClass(string FilePath)
        {
            this.Folder = FilePath.Substring(0, FilePath.LastIndexOf(@"\"));
            CreateFile();
        }

        /// <summary>
        /// Begin the watch on given
        /// </summary>
        public void StartWatch()
        {
            FSW.Path = Folder;
            FSW.Created += FSW_Created;
            FSW.Changed += FSW_Changed;
            FSW.Renamed += FSW_Renamed;
            FSW.Deleted += FSW_Deleted;
            FSW.Error += FSW_Error;
            FSW.EnableRaisingEvents = true;
            Status = WatchStatus.Started;
            using (StreamWriter w = File.AppendText(Folder + loc + @"\" + file))
            {
                w.WriteLine();
                w.WriteLine("-------------------------------");
                w.WriteLine(string.Format("Watcher start at {0} {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString()));
                
                w.Flush();
                w.Dispose();
                GC.Collect();
                GC.Collect();
            }
        }

        public void StopWatch()
        {
            FSW.EnableRaisingEvents = false;
            Status = WatchStatus.Stopped;
            using (StreamWriter w = File.AppendText(Folder + loc + @"\" + file))
            {
                w.WriteLine(string.Format("Watcher end at {0} {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString()));
                w.WriteLine("-------------------------------");
                w.WriteLine();

                w.Flush();
                w.Dispose();
                GC.Collect();
                GC.Collect();
            }
            FSW.Dispose();
            GC.Collect();
        }

        #region FileSystemWatcher event
        private void FSW_Created(object sender, FileSystemEventArgs e)
        {
            FileInfo fi = new FileInfo(e.FullPath);
            if (!((fi.Attributes | FileAttributes.Hidden) == fi.Attributes))
            {
                WriteLog(string.Format("Created \"{0}\"", e.FullPath));
                if (LogToDataBase)
                    WriteDB(Action.Create, e.FullPath);
            }
        }

        private void FSW_Changed(object sender, FileSystemEventArgs e)
        {
            try
            {
                FileInfo fi = new FileInfo(e.FullPath);
                if (!((fi.Attributes | FileAttributes.Hidden) == fi.Attributes))
                {
                    //prevent changed event called twice
                    FSW.EnableRaisingEvents = false;

                    WriteLog(string.Format("Changed \"{0}\"", e.FullPath));
                    if (LogToDataBase)
                        WriteDB(Action.Change, e.FullPath);
                }
            }
            finally
            {
                FSW.EnableRaisingEvents = true;
            }
        }

        private void FSW_Renamed(object sender, RenamedEventArgs e)
        {
            FileInfo fi = new FileInfo(e.FullPath);
            if (!((fi.Attributes | FileAttributes.Hidden) == fi.Attributes))
            {
                WriteLog(string.Format("Renamed \"{0}\" to \"{1}\"", e.OldFullPath, e.FullPath));

                if (LogToDataBase)
                    WriteDB(Action.Rename, e.OldFullPath+" to "+ e.FullPath);
            }
        }

        private void FSW_Deleted(object sender, FileSystemEventArgs e)
        {
            WriteLog(string.Format("Deleted \"{0}\"", e.FullPath));
            if (LogToDataBase)
                WriteDB(Action.Delete, e.FullPath);
        }

        private void FSW_Error(object sender, ErrorEventArgs e)
        {
            WriteLog(string.Format("Error -  \"{0}\"", e.GetException().Message));
            if (LogToDataBase)
                WriteDB(Action.Error, e.GetException().Message);
        }
        #endregion

        /// <summary>
        /// Generate a hidden file for logging if file not exist.
        /// </summary>
        private void CreateFile()
        {
            try
            {
                if (!Directory.Exists(Folder + loc))
                {
                    var di = Directory.CreateDirectory(Folder + loc);
                    di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;

                }
                //check file exist
                if (!File.Exists(Folder + loc + @"\" + file))
                {
                    using (var strm = File.Create(Folder + loc + @"\" + file))
                    {
                        File.SetAttributes(Folder + loc + @"\" + file, FileAttributes.Hidden);
                        Byte[] info = new UTF8Encoding(true).GetBytes(string.Format("File created {0} {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString()));
                        // Add some information to the file.
                        strm.Write(info, 0, info.Length);

                        strm.Flush();
                        strm.Close();
                        strm.Dispose();
                        GC.Collect();
                        GC.Collect();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Create File : " + ex.Message);
            }
        }

        /// <summary>
        /// Write the given message into log file
        /// </summary>
        /// <param name="msg">Message to be written</param>
        private void WriteLog(string msg)
        {
            try
            {
                using (StreamWriter w = File.AppendText(Folder + loc + @"\" + file))
                {
                    w.WriteLine(string.Format("{0} {1} : {2}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString(), msg));

                    w.Flush();
                    w.Dispose();
                    GC.Collect();
                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Write Log : " + ex.Message);
            }
        }

        private void WriteDB(Action act, string msg)
        {
            try
            {
                string conn = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection sqlcon = new SqlConnection(conn))
                {
                    sqlcon.Open();
                    using (SqlCommand cmd = new SqlCommand("insert into logging values(@time,@act,@msg,@mac,@ins)", sqlcon))
                    {
                        cmd.Parameters.AddWithValue("@time", DateTime.Now);
                        cmd.Parameters.AddWithValue("@act", act.ToString());
                        cmd.Parameters.AddWithValue("msg", msg);
                        cmd.Parameters.AddWithValue("mac", GetMACAddress());
                        cmd.Parameters.AddWithValue("ins", "WatchClass");
                        cmd.ExecuteNonQuery();
                    }
                    sqlcon.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("WriteDB : " + ex.Message);
            }
        }

        public string GetMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card  
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            }
            return sMacAddress;
        }
    }
}
