﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Documents;

using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace LearnPDF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string filename;
        private List<string> allImg = new List<string>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Btn_Browse_Click(object sender, RoutedEventArgs e)
        {
            GC.Collect();
            txtPDFval.Document.Blocks.Clear();
            Microsoft.Win32.FileDialog fdpdf = new Microsoft.Win32.OpenFileDialog();
            fdpdf.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
            fdpdf.Filter = "PDF File (*.pdf;)|*.pdf;";
            bool? yn = fdpdf.ShowDialog();
            if (yn == true)
                filename = fdpdf.FileName;
            else
                return;
            System.Windows.MessageBox.Show(filename);
            PdfReader reader = new PdfReader(filename);
            StringBuilder sb = new StringBuilder();
            try
            {

                ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                for (int i = 0; i < reader.NumberOfPages; i++)
                {
                    string text = PdfTextExtractor.GetTextFromPage(reader, i + 1, strategy);
                    sb.Append(text.Trim());
                }
                var img = PdfImageExtractor.ExtractImages(filename);


                var directory = System.IO.Path.GetDirectoryName(filename);

                foreach (var name in img.Keys)
                {
                    try
                    {
                        //2nd time occur error as it cannot access the existed file and rewrite it
                        img[name].Save(System.IO.Path.Combine(directory, name));
                    }
                    catch { }
                }
                // Check if file exists with its full path    
                foreach (var name in img.Keys)
                {
                    if (File.Exists(System.IO.Path.Combine(directory, name)))
                    {
                        var src = System.IO.Path.Combine(directory, name);
                        if(!allImg.Contains(src))
                            allImg.Add(src);
                        // If file found
                        File.SetAttributes(src, File.GetAttributes(System.IO.Path.Combine(directory, name)) | FileAttributes.Hidden);
                        System.Windows.Documents.Paragraph para = new System.Windows.Documents.Paragraph();

                        //print with Uri
                        //BitmapImage bitmap = new BitmapImage(new Uri(src));
                        //System.Windows.Controls.Image image = new System.Windows.Controls.Image()
                        //{
                        //    Source = bitmap,
                        //    Width = 100
                        //};
                        //para.Inlines.Add(image);

                        //print without Uri
                        BitmapImage bp = new BitmapImage();
                        using (var stream = File.OpenRead(System.IO.Path.Combine(directory, name)))
                        {
                            bp.BeginInit();
                            bp.CacheOption = BitmapCacheOption.OnLoad;
                            bp.StreamSource = stream;
                            System.Windows.Controls.Image image2 = new System.Windows.Controls.Image()
                            {
                                Source = bp,
                                Width = 100
                            };

                            para.Inlines.Add(image2);
                            bp.EndInit();
                        }


                        fld.Blocks.Add(para);
                    }
                    else Console.WriteLine("File not found");
                }

            }
            catch (IOException ioExp)
            {
                Console.WriteLine(ioExp.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            System.Windows.MessageBox.Show(sb.ToString());
            txtPDFval.AppendText(sb.ToString());
            reader.Close();

            foreach (var name in allImg)
            {
                if (File.Exists(name))
                {
                    File.Delete(name);
                }
            }
        }

        private void TxtPDFval_SelectionChanged(object sender, RoutedEventArgs e)
        {

            //MessageBox.Show(txtPDFval.Selection.Text);
        }

        private void Btn_Save_Click(object sender, RoutedEventArgs e)
        {
            Document doc = new Document(PageSize.LETTER, 10, 10, 30, 10);
            PdfWriter write = PdfWriter.GetInstance(doc, new FileStream(filename, FileMode.Create));

            doc.Open();
            //write text into document
            string richText = new TextRange(txtPDFval.Document.ContentStart, txtPDFval.Document.ContentEnd).Text;
            iTextSharp.text.Paragraph pr = new iTextSharp.text.Paragraph(richText);
            doc.Add(pr);

            foreach (Block block in txtPDFval.Document.Blocks)
            {
                if (block is System.Windows.Documents.Paragraph)
                {
                    System.Windows.Documents.Paragraph paragraph = (System.Windows.Documents.Paragraph)block;
                    foreach (Inline inline in paragraph.Inlines)
                    {
                        if (inline is InlineUIContainer)
                        {
                            InlineUIContainer uiContainer = (InlineUIContainer)inline;
                            if (uiContainer.Child is System.Windows.Controls.Image)
                            {
                                //retrieve via image URI
                                System.Windows.Controls.Image image = (System.Windows.Controls.Image)uiContainer.Child;
                                //var pa = image.Source.ToString();
                                //pa = pa.Substring(pa.IndexOf('C'));
                                //pa = pa.Replace('/', '\\');
                                //if (!allImg.Contains(pa))
                                //    allImg.Add(pa);
                                //using (var imageStraeam = new FileStream(pa, FileMode.Open, FileAccess.Read))
                                //{
                                //    var img = iTextSharp.text.Image.GetInstance(imageStraeam);
                                //    doc.Add(img);
                                //}

                                //retrieve via ... don't know?
                                var imgSrc = image.Source;
                                var imgbyte = ImageSourceToBytes(new PngBitmapEncoder(),imgSrc);
                                var imgBit = iTextSharp.text.Image.GetInstance(imgbyte);
                                doc.Add(imgBit);
                            }
                        }
                        else
                        {
                            //System.Windows.MessageBox.Show(inline.GetType().ToString());
                        }
                    }
                }
            }


            doc.Close();

            System.Windows.MessageBox.Show("Save complete");
            //((System.Windows.Controls.Button)sender).IsEnabled = false;
            
            txtPDFval.Document.Blocks.Clear();
            filename = null;
            GC.Collect();
            GC.Collect();
        }

        private void Btn_SaveAs_Click(object sender, RoutedEventArgs e)
        {
            string folder;
            string tempfilename;
            //request store location
            FolderBrowserDialog fbdFolder = new FolderBrowserDialog();
            fbdFolder.RootFolder = Environment.SpecialFolder.MyComputer;
            if (fbdFolder.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                folder = fbdFolder.SelectedPath;
            else
                return;
            //request new filename
            InputDialog idFileName = new InputDialog("Enter file name", "New File Name");
            bool? yn = idFileName.ShowDialog();
            if (yn == true)
                tempfilename = folder + "\\" + idFileName.inputText;
            else
                return;

            Document doc = new Document(PageSize.LETTER, 10, 10, 30, 10);
            PdfWriter write = PdfWriter.GetInstance(doc, new FileStream(tempfilename + ".pdf", FileMode.Create));

            doc.Open();
            string richText = new TextRange(txtPDFval.Document.ContentStart, txtPDFval.Document.ContentEnd).Text;
            iTextSharp.text.Paragraph pr = new iTextSharp.text.Paragraph(richText);
            doc.Add(pr);
            doc.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            txtPDFval.Document.Blocks.Clear();            
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            GC.Collect();
            GC.Collect();
        }
        public byte[] ImageSourceToBytes(BitmapEncoder encoder, ImageSource imageSource)
        {
            byte[] bytes = null;
            var bitmapSource = imageSource as BitmapSource;

            if (bitmapSource != null)
            {
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    bytes = stream.ToArray();
                }
            }

            return bytes;
        }


        #region tool panel
        private void TxtFontSize_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                try
                {
                    double size= double.Parse(txtFontSize.Text);
                    TextRange allText = new TextRange(txtPDFval.Document.ContentStart, txtPDFval.Document.ContentEnd);

                    allText.ApplyPropertyValue(TextElement.FontSizeProperty, size);
                }
                catch (FormatException) { System.Windows.MessageBox.Show("Please enter number only."); }
            }
        }



        #endregion
    }
}
