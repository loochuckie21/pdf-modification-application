﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace LearnPDF
{
    /// <summary>
    /// Interaction logic for EditTextDialog.xaml
    /// </summary>
    public partial class EditTextDialog : Window
    {
        private List<string[]> contents;
        private List<ContentClass> cc = new List<ContentClass>();
        int totalword = 0;

        public EditTextDialog()
        {
            InitializeComponent();
        }
        //public EditTextDialog(List<string[]> data, string highlight)
        //{
        //    Contents = data;
        //    InitializeComponent();
        //    StringBuilder temp = new StringBuilder();
        //    for (int pg = 0; pg < data.Count; pg++)
        //    {
        //        string[] texts = data.ElementAt(pg);
        //        for (int i = 0; i < texts.Length; i++)
        //        {
        //            temp.Append(texts[i].Trim() + "\n");
        //        }
        //        if (pg + 1 != data.Count)
        //        {
        //            temp.Append("\n[PageBreak]\n");
        //        }
        //    }
        //    mainContentBox.AppendText(temp.ToString());

        //    if (highlight != null)
        //    {
        //        bool found = false;
        //        for (int pg = 0; pg < data.Count && !found; pg++)
        //        {
        //            string[] texts = data.ElementAt(pg);
        //            for (int ln = 0; ln < texts.Length && !found; ln++)
        //            {
        //                string cur = texts[ln].Trim();
        //                if (!cur.Equals(highlight.Trim()))
        //                    totalword += cur.Length + 4;//that 3 represent \r\n, each also symbol also counted
        //                else
        //                    found = true;
        //            }
        //        }


        //        TextPointer text = mainContentBox.Document.ContentStart;
        //        while (text.GetPointerContext(LogicalDirection.Forward) != TextPointerContext.Text)
        //        {
        //            text = text.GetNextContextPosition(LogicalDirection.Forward);
        //        }
        //        TextPointer startPos = text.GetPositionAtOffset(totalword);//eg: |ABC\ , | is at 0,\ at 3
        //        TextPointer endPos = text.GetPositionAtOffset(totalword + highlight.Trim().Length);
        //        var textRange = new TextRange(startPos, endPos);

        //        var a = text.GetOffsetToPosition(text);
        //        var b = text.GetOffsetToPosition(startPos);
        //        var c = text.GetOffsetToPosition(endPos);
        //        var s = text.GetOffsetToPosition(textRange.Start);
        //        var e = text.GetOffsetToPosition(textRange.End);
        //        var t = textRange.Text;

        //        //mainContentBox.Selection.Select(startPos, endPos);
        //        textRange.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.ExtraBold);
        //        //textRange.Select(startPos, endPos);
        //    }
        //}


        public EditTextDialog(List<ContentClass> data, string highlight, string keyword)
        {
            cc = data;
            InitializeComponent();

            StringBuilder temp = new StringBuilder();
            int pg = 0;
            foreach (ContentClass con in cc)
            {
                if (pg != con.Page)
                {
                    temp.Append("\n[PageBreak]\n");
                    pg++;
                }
                temp.Append(con.Words + "\n");
            }

            mainContentBox.AppendText(temp.ToString());
            TextPointer text = mainContentBox.Document.ContentStart;
            while (text.GetPointerContext(LogicalDirection.Forward) != TextPointerContext.Text)
            {
                text = text.GetNextContextPosition(LogicalDirection.Forward);
            }

            mainContentBox.Focus();
            //mainContentBox.SelectAll();
            mainContentBox.Selection.Select(text.DocumentStart, text.DocumentEnd);
            mainContentBox.SelectionBrush = Brushes.Violet;
            /*bool align = false;
            var see = new TextRange(mainContentBox.Document.ContentStart, mainContentBox.Document.ContentEnd).Text;
            if (highlight != null)
            {
                foreach (ContentClass con in cc)
                {
                    align = false;
                    if (con.KeyIndexes.Count != 0)
                    {
                        int totaltemp = totalword;
                        TextPointer text = mainContentBox.Document.ContentStart;
                        while (text.GetPointerContext(LogicalDirection.Forward) != TextPointerContext.Text)
                        {
                            text = text.GetNextContextPosition(LogicalDirection.Forward);
                        }
                        foreach (int ind in con.KeyIndexes)
                        {
                            TextPointer startPos = text.GetPositionAtOffset(ind + totaltemp);
                            TextPointer endPos = text.GetPositionAtOffset(ind + totaltemp + keyword.Length);
                            var textRange = new TextRange(startPos, endPos);

                            textRange.Select(startPos, endPos);
                            mainContentBox.Selection.Select(startPos, endPos);
                            mainContentBox.SelectionBrush=Brushes.Yellow;

                            var a = text.GetOffsetToPosition(text);
                            var tpStart = text.GetOffsetToPosition(startPos);
                            var tpEnd = text.GetOffsetToPosition(endPos);
                            var rangeS = text.GetOffsetToPosition(textRange.Start);
                            var rangeE = text.GetOffsetToPosition(textRange.End);
                            var t = textRange.Text;


                        }
                    }

                    
                    totalword += con.Words.Trim().Length + 4;
                }


                //TextPointer text = mainContentBox.Document.ContentStart;
                //while (text.GetPointerContext(LogicalDirection.Forward) != TextPointerContext.Text)
                //{
                //    text = text.GetNextContextPosition(LogicalDirection.Forward);
                //}
                //TextPointer startPos = text.GetPositionAtOffset(totalword);//eg: |ABC\ , | is at 0,\ at 3
                //TextPointer endPos = text.GetPositionAtOffset(totalword + highlight.Trim().Length);
                //var textRange = new TextRange(startPos, endPos);

                //var a = text.GetOffsetToPosition(text);
                //var b = text.GetOffsetToPosition(startPos);
                //var c = text.GetOffsetToPosition(endPos);
                //var s = text.GetOffsetToPosition(textRange.Start);
                //var e = text.GetOffsetToPosition(textRange.End);
                //var t = textRange.Text;

                ////mainContentBox.Selection.Select(startPos, endPos);
                //textRange.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.ExtraBold);
                ////textRange.Select(startPos, endPos);
            }*/
        }
        public List<string[]> Contents { get => contents; set => contents = value; }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            e.Handled = true;
        }

        private void BtnDone_Click(object sender, RoutedEventArgs e)
        {
            //.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries)
            string rs = new TextRange(mainContentBox.Document.ContentStart, mainContentBox.Document.ContentEnd).Text;
            string[] splitPage = rs.Split(new string[] { "[PageBreak]" }, StringSplitOptions.RemoveEmptyEntries);
            List<string[]> edited = new List<string[]>();
            for (int i = 0; i < splitPage.Length; i++)
            {
                string[] splitLine = splitPage[i].Split(new string[] { "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries);
                for (int line = 0; line < splitLine.Length; line++)
                {
                    splitLine[line] = Regex.Replace(splitLine[line], @"\s\s+", @"");
                }
                splitLine = splitLine.Where(str => !string.IsNullOrWhiteSpace(str)).ToArray();
                splitLine = splitLine.Where(str => !string.IsNullOrEmpty(str)).ToArray();
                edited.Add(splitLine);
            }
            Contents = edited;

            DialogResult = true;
            e.Handled = true;
        }

        private void BtnInsPB_Click(object sender, RoutedEventArgs e)
        {
            string lPastingText = "\n[PageBreak]\n";

            mainContentBox.CaretPosition.GetPositionAtOffset(0, LogicalDirection.Backward).InsertTextInRun(lPastingText);
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult ahaha = MessageBox.Show("Confirm clear everything?", "Confirm?", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (ahaha.Equals(MessageBoxResult.Yes))
                mainContentBox.Document.Blocks.Clear();
            else
                return;
        }
    }
}
