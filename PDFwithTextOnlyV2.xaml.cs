﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Forms;
using MessageBox = System.Windows.MessageBox;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Threading;

namespace LearnPDF
{
    /// <summary>
    /// Interaction logic for PDFwithTextOnlyV2.xaml
    /// </summary>

    enum MenuStyleItem
    {
        ShopName,
        Category,
        SubCategory,
        Items
    }


    public partial class PDFwithTextOnlyV2 : Window
    {
        private readonly string[] TEXTHINTS = 
        { "Click on \"Add Page\" to start a new PDF.",
          "Browse any PDF file to view and edit the content.",
          "Convert from text to menu form will clear out the font you set previously.",
          "Hit the \"Clear All\" button to reset everything.",
          "The search box will appear on the most left-bottom corner when you click on \"Find\" button.",
          "You can monitor what happen to your folder if you start either service and/or watch.",
          "Service can be remain even after you close me while watch will just gone.",
          "You can only save the menu to the database.",
          "You can\'t directly hit save button if you retrieve from database! You only can save as.",
          "You need to tell what money do you use in your text to convert to menu.",
          "You can set all text to same font if you set the default font style."
        };

        private readonly string[] MENUHINTS =
        { "You can\'t change both category name and sub-category name! But you can do it in text form.",
          "You can set font to all categorized item! But not each single of it.",
          "Tell the me how many lines you wish to have for each page when convert menu to text form.",
          "Remember to save your things, I won\'t save it for you! Hmmpm.",
          "Sad thing to tell you : you can\'t use find and replace feature on here!",
          "There is no page here, you can\'t select page or add page here."
        };

        private string filename;
        private List<ContentClass> pdfContent = new List<ContentClass>();
        private List<int> searchInd = new List<int>();
        private string currency;
        private int IndPointer = -1;
        private int selectedInd = -1;
        private int pageSearchCount = 0;
        private int docSearchCount = 0;
        private bool lastIsNext = false;
        private bool lastIsPrev = false;
        private int currentPageInd = -1;
        private MenuClass menuContent = new MenuClass();
        private Dictionary<string, System.Drawing.Font> styling = new Dictionary<string, System.Drawing.Font>();
        private DispatcherTimer hinttime = new DispatcherTimer();
        private ServiceClass sc = new ServiceClass("BService");
        private FileWatcherClass FWC;

        private List<string> hintList = new List<string>();

        public PDFwithTextOnlyV2()
        {
            InitializeComponent();
            FontFactory.RegisterDirectories();
            if (!sc.IsServiceInstalled())
                MessageBox.Show("LearnService service is not found. If you wish to use the service, please seek for developer to install the service.");
            else if (sc.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.Running)
            {
                sc.StopService(5000);
                MessageBox.Show("Service stopped.");
            }
            hinttime.Interval = new TimeSpan(0, 0, 8);
            hinttime.Tick += Hinttime_Tick;
            hinttime.Start();
        }

        private void Hinttime_Tick(object sender, EventArgs e)
        {
            if (hintList.Count == 0)
                GenHint();
            lblHint.Content = hintList[0];
            hintList.RemoveAt(0);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (sc.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.Running)
                    if (MessageBox.Show("Stop the service after exit?", "Service", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        sc.StopService(5000);
                if (FWC != null)
                    if (FWC.Status == WatchStatus.Started)
                        FWC.StopWatch();
                hinttime.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                e.Cancel = false;
                return;
            }
        }

        #region menu events
        private void BtnBrowse_Click(object sender, RoutedEventArgs e)
        {
            GC.Collect();
            cmbPages.Items.Clear();
            pdfContent.Clear();
            Microsoft.Win32.FileDialog fdpdf = new Microsoft.Win32.OpenFileDialog();
            fdpdf.InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString();
            fdpdf.Filter = "PDF File (*.pdf;)|*.pdf;";
            bool? yn = fdpdf.ShowDialog();
            if (yn == true)
                filename = fdpdf.FileName;
            else
                return;

            PdfReader reader = new PdfReader(filename);
            try
            {
                for (int pg = 0; pg < reader.NumberOfPages; pg++)
                {
                    string text = PdfTextExtractor.GetTextFromPage(reader, pg + 1);
                    string[] val = text.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
                    cmbPages.Items.Add(pg + 1);
                    for (int line = 0; line < val.Length; line++)
                    {
                        val[line] = Regex.Replace(val[line], @"\s\s+", @" ");        //replace all large whitespace to empty
                        val[line] = val[line].Trim();
                        pdfContent.Add(new ContentClass(pg, line, val[line]));
                    }
                }
                currentPageInd = -1;
                cmbPages.SelectedIndex = 0;//cmbPage_selection_changed contains WriteContent
                reader.Close();
                HideSearch.Visibility = Visibility.Hidden;
                cmbPages.IsEnabled = true;
                btnFind.IsEnabled = true;
                btnSave.IsEnabled = true;
                btnSaveAs.IsEnabled = true;
                btnSetDefaultStyle.IsEnabled = true;
                btnSetStyle.IsEnabled = true;
                btnService.IsEnabled = true;
                btnWatch.IsEnabled = true;
                btnConnect.IsEnabled = false;
                btnConvertMenu.IsEnabled = true;
                GenHint();
            }
            catch (IOException ioExp)
            {
                MessageBox.Show(ioExp.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnFind_Click(object sender, RoutedEventArgs e)
        {
            if (HideSearch.Visibility == Visibility.Hidden)
                HideSearch.Visibility = Visibility.Visible;
            else
                HideSearch.Visibility = Visibility.Hidden;
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {//RichTextBox part
            if (filename != null)
            {
                if (MainContentHolder.Visibility == Visibility.Visible)
                {
                    StoreData();
                    Document doc = new Document(PageSize.LETTER, 30, 30, 30, 30);
                    PdfWriter write = PdfWriter.GetInstance(doc, new FileStream(filename, FileMode.Create));

                    doc.Open();

                    List<ContentClass> temp = pdfContent;
                    for (int pg = 0; pg <= cmbPages.Items.Count; pg++)
                    {
                        for (int ln = 0; ln < temp.Count; ln++)
                        {
                            bool found = false;
                            for (int data = 0; data < temp.Count && !found; data++)
                            {
                                if (temp[data].Line == ln && temp[data].Page == pg) //see whether it is in right line,right page
                                {
                                    found = true;// stop the loop once found
                                    iTextSharp.text.Paragraph pr = temp[data].GetPdfParagraph();
                                    doc.Add(pr);
                                }
                            }
                        }
                        doc.NewPage();
                    }
                    doc.Close();
                }// end RichTextBox
                else
                {//save menu format
                    Document doc = new Document(PageSize.LETTER, 30, 30, 30, 30);
                    PdfWriter write = PdfWriter.GetInstance(doc, new FileStream(filename, FileMode.Create));

                    doc.Open();
                    iTextSharp.text.Font shopnamefont = new iTextSharp.text.Font();
                    if (menuContent.DefaultFont != null)
                    {
                        shopnamefont = FontFactory.GetFont(menuContent.DefaultFont.FontFamily.Name, menuContent.DefaultFont.Size);
                        if (menuContent.DefaultFont.Italic)
                            shopnamefont.SetStyle(iTextSharp.text.Font.ITALIC);
                        if (menuContent.DefaultFont.Bold)
                            shopnamefont.SetStyle(iTextSharp.text.Font.BOLD);
                    }
                    iTextSharp.text.Paragraph ShopName = new iTextSharp.text.Paragraph(menuContent.ShopName, shopnamefont);
                    ShopName.Alignment = Element.ALIGN_CENTER;

                    doc.Add(ShopName);
                    foreach (var category in menuContent.Menus)
                    {
                        iTextSharp.text.Font catefont = new iTextSharp.text.Font();
                        if (category.DefaultFont != null)
                        {
                            catefont = FontFactory.GetFont(category.DefaultFont.FontFamily.Name, category.DefaultFont.Size);
                            if (category.DefaultFont.Italic)
                                catefont.SetStyle(iTextSharp.text.Font.ITALIC);
                            if (category.DefaultFont.Bold)
                                catefont.SetStyle(iTextSharp.text.Font.BOLD);
                        }
                        iTextSharp.text.Paragraph catepara = new iTextSharp.text.Paragraph(category.Category, catefont);
                        doc.Add(catepara);
                        foreach (var subcategory in category.Subcategories)
                        {
                            doc.Add(new iTextSharp.text.Paragraph(" "));
                            doc.Add(subcategory.getSubCatePDFTable(currency));
                        }
                    }
                    doc.Close();
                }
                MessageBox.Show("Save complete");
                GC.Collect();
                GC.Collect();
            }
            else
            {
                BtnSaveAs_Click(sender, e);
            }
        }

        private void BtnSaveAs_Click(object sender, RoutedEventArgs e)
        {
            string folder;
            //request store location
            FolderBrowserDialog fbdFolder = new FolderBrowserDialog();
            fbdFolder.RootFolder = Environment.SpecialFolder.MyComputer;
            if (fbdFolder.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                folder = fbdFolder.SelectedPath;
            else
                return;
            //request new filename
            InputDialog idFileName = new InputDialog("Enter file name", "New File Name", "FileName.pdf");
            bool? yn = idFileName.ShowDialog();
            if (yn == true)
                filename = folder + "\\" + idFileName.inputText;
            else
                return;
            if (MainContentHolder.Visibility == Visibility.Visible)
            {
                StoreData();
                Document doc = new Document(PageSize.LETTER, 30, 30, 30, 30);
                PdfWriter write = PdfWriter.GetInstance(doc, new FileStream(filename, FileMode.Create));

                doc.Open();

                List<ContentClass> temp = pdfContent;
                for (int pg = 0; pg <= cmbPages.Items.Count; pg++)
                {
                    for (int ln = 0; ln < temp.Count; ln++)
                    {
                        bool found = false;
                        for (int data = 0; data < temp.Count && !found; data++)
                        {
                            if (temp[data].Line == ln && temp[data].Page == pg) //see whether it is in right line,right page
                            {
                                found = true;// stop the loop once found
                                iTextSharp.text.Paragraph pr = temp[data].GetPdfParagraph();
                                doc.Add(pr);
                            }
                        }
                    }
                    doc.NewPage();
                }
                doc.Close();
            }// end RichTextBox
            else
            {//save menu format
                Document doc = new Document(PageSize.LETTER, 30, 30, 30, 30);
                PdfWriter write = PdfWriter.GetInstance(doc, new FileStream(filename, FileMode.Create));

                doc.Open();
                iTextSharp.text.Font shopnamefont = new iTextSharp.text.Font();
                if (menuContent.DefaultFont != null)
                {
                    shopnamefont = FontFactory.GetFont(menuContent.DefaultFont.FontFamily.Name, menuContent.DefaultFont.Size);
                    if (menuContent.DefaultFont.Italic)
                        shopnamefont.SetStyle(iTextSharp.text.Font.ITALIC);
                    if (menuContent.DefaultFont.Bold)
                        shopnamefont.SetStyle(iTextSharp.text.Font.BOLD);
                }
                iTextSharp.text.Paragraph ShopName = new iTextSharp.text.Paragraph(menuContent.ShopName, shopnamefont);
                ShopName.Alignment = Element.ALIGN_CENTER;

                doc.Add(ShopName);
                foreach (var category in menuContent.Menus)
                {
                    iTextSharp.text.Font catefont = new iTextSharp.text.Font();
                    if (category.DefaultFont != null)
                    {
                        catefont = FontFactory.GetFont(category.DefaultFont.FontFamily.Name, category.DefaultFont.Size);
                        if (category.DefaultFont.Italic)
                            catefont.SetStyle(iTextSharp.text.Font.ITALIC);
                        if (category.DefaultFont.Bold)
                            catefont.SetStyle(iTextSharp.text.Font.BOLD);
                    }
                    iTextSharp.text.Paragraph catepara = new iTextSharp.text.Paragraph(category.Category, catefont);
                    doc.Add(catepara);
                    foreach (var subcategory in category.Subcategories)
                    {
                        doc.Add(new iTextSharp.text.Paragraph(" "));
                        doc.Add(subcategory.getSubCatePDFTable(currency));
                    }
                }
                doc.Close();
            }
            if (filename != null)
            {
                btnService.IsEnabled = true;
                btnWatch.IsEnabled = true;
                btnSave.IsEnabled = true;
            }
            else
            {
                btnService.IsEnabled = false;
                btnWatch.IsEnabled = false;
                btnSave.IsEnabled = false;
            }
            MessageBox.Show("Saved to " + filename);
            GC.Collect();
            GC.Collect();
        }

        private void CmbPages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //RichTextBox part
            if (MainContent.Visible == true)
            {
                int sel = cmbPages.SelectedIndex;
                StoreData();

                WriteContent(sel);
                if (txtSearch.Text.Length >= 1)
                    TxtSearch_KeyUp(sender, null);
                currentPageInd = sel;
            }//end RichTextBox part
        }

        private void BtnAddPage_Click(object sender, RoutedEventArgs e)
        {
            StoreData();
            cmbPages.Items.Add(cmbPages.Items.Count + 1);
            MainContent.Clear();
            cmbPages.SelectedIndex = cmbPages.Items.Count - 1;
            if (MainContentHolder.Visibility == Visibility.Visible)
            {
                btnSaveAs.IsEnabled = true;
                btnSetDefaultStyle.IsEnabled = true;
                btnSetStyle.IsEnabled = true;
                btnConvertMenu.IsEnabled = true;
                btnFind.IsEnabled = true;
                cmbPages.IsEnabled = true;
            }
            if (filename != null)
            {
                btnService.IsEnabled = true;
                btnWatch.IsEnabled = true;
                btnSave.IsEnabled = true;
            }
            else
            {
                btnService.IsEnabled = false;
                btnWatch.IsEnabled = false;
                btnSave.IsEnabled = false;
            }
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            cmbPages.Items.Clear();
            pdfContent = new List<ContentClass>();
            searchInd = new List<int>();
            currency = null;
            filename = null;
            IndPointer = -1;
            selectedInd = -1;
            pageSearchCount = 0;
            docSearchCount = 0;
            lastIsNext = false;
            lastIsPrev = false;
            currentPageInd = -1;
            menuContent = new MenuClass();
            styling = new Dictionary<string, System.Drawing.Font>();

            cmbPages.IsEnabled = false;
            btnFind.IsEnabled = false;
            btnSave.IsEnabled = false;
            btnSaveAs.IsEnabled = false;
            HideSearch.Visibility = Visibility.Hidden;
            MainContentHolder.Visibility = Visibility.Visible;
            MenuContentHolder.Visibility = Visibility.Hidden;
            textTool.Visibility = Visibility.Visible;
            btnSetDefaultStyle.IsEnabled = false;
            btnSetStyle.IsEnabled = false;
            menuTool.Visibility = Visibility.Hidden;
            btnService.IsEnabled = false;
            btnWatch.IsEnabled = false;
            btnConnect.IsEnabled = false;
            btnConvertMenu.IsEnabled = false;
            GenHint();
        }

        #endregion

        #region footer : search events
        private void TxtSearch_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            MainContent.SelectionStart = 0;
            MainContent.SelectionLength = MainContent.Text.Length;
            MainContent.SelectionBackColor = System.Drawing.Color.White;
            string search = txtSearch.Text;
            try
            {
                int ind = 0;
                pageSearchCount = 0;
                docSearchCount = 0;
                if (search.Length >= 1)
                {
                    searchInd.Clear();
                    IndPointer = -1;
                    var iaa = MainContent.Text.LastIndexOf(search);
                    while (ind <= MainContent.Text.LastIndexOf(search))
                    {
                        MainContent.Find(search, ind, MainContent.TextLength, RichTextBoxFinds.MatchCase); // select the text
                        MainContent.SelectionBackColor = System.Drawing.Color.Aqua; // set select background
                        ind = MainContent.Text.IndexOf(search, ind) + search.Length;// +search.Length to prevent get the current searched location
                        searchInd.Add(ind - search.Length);//store the starting location
                        pageSearchCount++;
                    }
                    string tempContent = "";
                    foreach (ContentClass cc in pdfContent)
                        tempContent += cc.Words;
                    int tempInd = 0;
                    while (tempInd <= tempContent.LastIndexOf(search))
                    {
                        docSearchCount++;
                        tempInd = tempContent.IndexOf(search, tempInd) + search.Length;
                    }
                    if (docSearchCount != 0)
                    {
                        //selectedInd = searchInd[0];
                        btnFindNext.IsEnabled = true;
                        btnReplace.IsEnabled = true;
                        btnReplaceAll.IsEnabled = true;
                        btnFindPrev.IsEnabled = true;
                    }
                    else
                    {
                        btnFindPrev.IsEnabled = false;
                        btnFindNext.IsEnabled = false;
                        btnReplace.IsEnabled = false;
                        btnReplaceAll.IsEnabled = false;
                    }
                    lblSearchRS.Text = string.Format("Search : \"{0}\", {1} occurrence(s) in this page, {2} occurrence(s) in this file.", search, pageSearchCount, docSearchCount);
                }
                else { lblSearchRS.Text = "Search :"; }
            }
            catch (Exception ex) { MessageBox.Show("Error here : " + ex.Message); }


        }

        private void BtnReplace_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var theString = MainContent.Text;
                var aStringBuilder = new StringBuilder(theString);
                aStringBuilder.Remove(selectedInd, txtSearch.Text.Length);
                aStringBuilder.Insert(selectedInd, txtReplace.Text);
                theString = aStringBuilder.ToString();
                MainContent.Text = theString;
                TxtSearch_KeyUp(sender, null);//refresh the contents

                MainContent.Find(txtReplace.Text, selectedInd, MainContent.Text.Length, RichTextBoxFinds.MatchCase);//set highlight to new replaced text
                MainContent.SelectionBackColor = System.Drawing.Color.LightCoral;
                MainContent.Focus();
            }
            catch (Exception)
            {
                MessageBox.Show("Please select the word you wish to replace");
            }
        }

        private void BtnReplaceAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (ContentClass cc in pdfContent)
                {
                    cc.Words = cc.Words.Replace(txtSearch.Text, txtReplace.Text);
                }
                WriteContent(cmbPages.SelectedIndex);
                TxtSearch_KeyUp(sender, null);

                int temppo = 0;
                while (temppo <= MainContent.Text.LastIndexOf(txtReplace.Text))
                {
                    MainContent.Find(txtReplace.Text, 0, MainContent.Text.Length, RichTextBoxFinds.MatchCase);
                    MainContent.SelectionBackColor = System.Drawing.Color.LightCoral;
                    MainContent.Focus();
                    temppo = MainContent.Text.IndexOf(txtReplace.Text, temppo) + txtReplace.Text.Length;
                }
            }
            catch { }

        }

        private void BtnFindNext_Click(object sender, RoutedEventArgs e)
        {
            if (lastIsPrev)
            {
                IndPointer += 2;
                lastIsPrev = false;
            }
            if (IndPointer < 0 && searchInd.Count > 0)
                IndPointer = 0;
            if (IndPointer >= searchInd.Count || IndPointer < 0)
            {
                if (cmbPages.SelectedIndex + 1 == cmbPages.Items.Count || pageSearchCount == docSearchCount)
                {
                    MessageBox.Show("Reached end of document.");
                    selectedInd = searchInd[IndPointer - 1];
                    MainContent.Find(txtSearch.Text, searchInd[IndPointer - 1], MainContent.Text.Length, RichTextBoxFinds.MatchCase);
                    MainContent.Focus();
                    return;
                }
                else
                {
                    cmbPages.SelectedIndex = cmbPages.SelectedIndex + 1; //trigger cmbPages_selection_changed()
                    IndPointer = 0;
                }
            }
            selectedInd = searchInd[IndPointer];
            int got = MainContent.Find(txtSearch.Text, selectedInd, MainContent.Text.Length, RichTextBoxFinds.MatchCase);

            MainContent.Focus();
            lastIsNext = true;
            IndPointer++;
        }

        private void BtnFindPrev_Click(object sender, RoutedEventArgs e)
        {
            if (lastIsNext)
            {
                IndPointer -= 2;
                lastIsNext = false;
            }
            if (IndPointer < 0)
            {
                if (cmbPages.SelectedIndex == 0 || pageSearchCount == docSearchCount)
                {
                    MessageBox.Show("Reached beginning of document.");
                    selectedInd = searchInd[0];
                    MainContent.Find(txtSearch.Text, searchInd[0], MainContent.Text.Length, RichTextBoxFinds.MatchCase);
                    MainContent.Focus();
                    return;
                }
                else
                {
                    cmbPages.SelectedIndex = cmbPages.SelectedIndex - 1; //trigger cmbPages_selection_changed()
                    IndPointer = searchInd.Count - 1;
                }
            }
            selectedInd = searchInd[IndPointer];
            int got = MainContent.Find(txtSearch.Text, selectedInd, MainContent.Text.Length, RichTextBoxFinds.MatchCase);

            MainContent.Focus();
            IndPointer--;
            lastIsPrev = true;
        }
        #endregion

        #region toolpanel events
        private void BtnSetFontStyle_Click(object sender, RoutedEventArgs e)
        {
            FontDialog fd = new FontDialog();
            fd.ShowColor = false;
            fd.ShowApply = false;
            if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                System.Drawing.Font f = fd.Font;
                MainContent.SelectionFont = f;
                int countLine = 0;
                int page = currentPageInd;
                string[] splitContent = MainContent.Text.Split(new char[] { '\n' });
                int lineindex = -1;
                for (int findLine = MainContent.SelectionStart; findLine >= 0;)//search the line
                {
                    lineindex = findLine;
                    findLine -= (splitContent[countLine].Length + 1);//+1 represent \n
                    countLine++;
                }
                countLine--;
                bool found = false;
                string selText = MainContent.SelectedText;
                for (int pdfdatacount = 0; pdfdatacount < pdfContent.Count && !found; pdfdatacount++)//find the line and page pdfcontent
                {
                    if (pdfContent[pdfdatacount].Page == page && pdfContent[pdfdatacount].Line == countLine)
                    {
                        while (selText.Length > 0)
                        {
                            string temp = pdfContent[pdfdatacount].Words.Substring(lineindex);
                            if (selText.Length > temp.Length)
                            {
                                selText = selText.Substring(temp.Length + 1);
                                pdfContent[pdfdatacount].AddTextStyle(lineindex, temp.Length, f);
                                lineindex = 0;
                                pdfdatacount++;
                            }
                            else
                            {
                                pdfContent[pdfdatacount].AddTextStyle(lineindex, selText.Length, f);
                                selText = selText.Substring(selText.Length);
                            }
                        }
                        found = true;
                    }
                }
                MainContent.Focus();
            }
        }

        private void BtnSetMenuStyle_Click(object sender, RoutedEventArgs e)
        {
            string selItem = cmbMenuItem.SelectedValue.ToString();
            FontDialog fd = new FontDialog();
            fd.ShowColor = false;
            fd.ShowApply = false;
            if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    switch (selItem)
                    {
                        case "Shop Name":
                            {
                                menuContent.DefaultFont = fd.Font;
                                txtMenuShopName.FontFamily = new System.Windows.Media.FontFamily(fd.Font.FontFamily.GetName(0).ToString());
                                txtMenuShopName.FontSize = fd.Font.Size;
                                txtMenuShopName.FontWeight = fd.Font.Bold ? FontWeights.Bold : FontWeights.Regular;
                                txtMenuShopName.FontStyle = fd.Font.Italic ? FontStyles.Italic : FontStyles.Normal;
                                break;
                            }
                        case "Category":
                            {
                                menuContent.Menus[0].DefaultFont = fd.Font;
                                cmbCate.FontFamily = new System.Windows.Media.FontFamily(fd.Font.FontFamily.GetName(0).ToString());
                                cmbCate.FontSize = fd.Font.Size;
                                cmbCate.FontWeight = fd.Font.Bold ? FontWeights.Bold : FontWeights.Regular;
                                cmbCate.FontStyle = fd.Font.Italic ? FontStyles.Italic : FontStyles.Normal;
                                break;
                            }
                        case "Sub-category":
                            {
                                menuContent.Menus[0].Subcategories[0].DefaultFont = fd.Font;
                                cmbSubCate.FontFamily = new System.Windows.Media.FontFamily(fd.Font.FontFamily.GetName(0).ToString());
                                cmbSubCate.FontSize = fd.Font.Size;
                                cmbSubCate.FontWeight = fd.Font.Bold ? FontWeights.Bold : FontWeights.Regular;
                                cmbSubCate.FontStyle = fd.Font.Italic ? FontStyles.Italic : FontStyles.Normal;
                                break;
                            }
                        case "Items":
                            {
                                menuContent.Menus[0].Subcategories[0].Items[0].DefaultFont = fd.Font;
                                dgMenu.FontFamily = new System.Windows.Media.FontFamily(fd.Font.FontFamily.GetName(0).ToString());
                                dgMenu.FontSize = fd.Font.Size;
                                dgMenu.FontWeight = fd.Font.Bold ? FontWeights.Bold : FontWeights.Regular;
                                dgMenu.FontStyle = fd.Font.Italic ? FontStyles.Italic : FontStyles.Normal;
                                break;
                            }
                        default:
                            {
                                throw new Exception("Selected item is not found");
                            }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Error occurred. \n\"{0}\"", ex.Message));
                }
            }
        }

        private void BtnSetDefaultStyle_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("This feature is still under construction.");
            //return;
            FontDialog fd = new FontDialog();
            fd.ShowColor = false;
            fd.ShowApply = false;
            if (MessageBox.Show("The font style set before will be cleared. Continue?", "Confirmation required",
                MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) == MessageBoxResult.Yes)
            {
                if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    System.Drawing.Font f = fd.Font;
                    MainContent.Font = f;
                    pdfContent[0].DefaultFont = f;
                    for (int i = 0; i < pdfContent.Count; i++)
                    {
                        pdfContent[i].Textrange.Clear();
                        pdfContent[i].Style.Clear();
                    }
                }
            }
        }

        private void BtnConvertMenu_Click(object sender, RoutedEventArgs e)
        {
            if (MainContentHolder.Visibility == Visibility.Visible)
            {//convert from text to menu form
                StoreData();
                InputDialog idCurrency = new InputDialog("Please enter the currency(according to the menu)", "Currency");
                bool? yn = idCurrency.ShowDialog();
                if (yn == true)
                    currency = idCurrency.inputText;
                else
                    return;
                cmbPages.IsEnabled = false;
                MainContentHolder.Visibility = Visibility.Hidden;
                MenuContentHolder.Visibility = Visibility.Visible;
                textTool.Visibility = Visibility.Hidden;
                menuTool.Visibility = Visibility.Visible;
                btnAddPage.IsEnabled = false;
                btnFind.IsEnabled = false;
                btnSave.IsEnabled = true;
                btnSaveAs.IsEnabled = true;
                btnConnect.IsEnabled = true;
                btnConvertMenu.Content = "Convert Text";
                try
                {
                    /*Questions :
                     1) how to determine shop name? 
                     -Solution : Take first row as shop name(accept) 
                                 Second page? If match first row, ignore (accept)
                                 remove each row and? (resolved)

                     2) how to determine category? 
                     -Solution : Take all caps as category(reconsider)
                                 What if they did not make it upper case? 
                                 How bout after filtering both shopname and menu item, it is category?
                                 not really acceptable, what if they write it for nothing?

                     3) how to determine sub-category?
                     -Solution : what now? all caps?next line caps? uhh
                                 Possible? toggle?
                                 no toggle, take next line determine whether it is all cap
                                 if it is capped, this as category, next as sub-category

                     3) how to determine menu item? 
                     -Solution : As long as there is "RM" letter, it is menu item? 
                                 How bout "Tormont" or wadever? follow up is double then settle.

                     4) how to determine pricing? 
                     -Solution : Split the menu item via "RM" and 2nd sure is pricing?
                     */
                    menuContent = new MenuClass();
                    List<string> failtoadd = new List<string>();
                    int falseMenuCount = 0;
                    int notAddedCount = 0;
                    int curCatInd = -1;
                    int curSubCatInd = -1;
                    for (int ind = 0; ind < pdfContent.Count; ind++)
                    {
                        string line = pdfContent[ind].Words.Trim();
                        if (menuContent.ShopName == null) //solve Question 1
                        {
                            menuContent.ShopName = line;
                        }
                        else if (menuContent.ShopName.Equals(line))
                        {//do nothing
                        }
                        else if (line.IndexOf(currency) != -1)
                        {
                            var storing = line.Split(new string[] { currency }, StringSplitOptions.RemoveEmptyEntries);
                            try
                            {
                                //menuContent.AddMenuItem(curCatInd,storing[0],double.Parse(storing[1]));
                                menuContent.AddMenuItem(curCatInd, curSubCatInd, storing[0], double.Parse(storing[1]));
                            }
                            catch (Exception)
                            {
                                //MessageBox.Show(ex.Message);
                                falseMenuCount++;
                                failtoadd.Add(line);
                            }
                        }
                        else if (!line.Any(c => char.IsLower(c)) && line != "")
                        {
                            if (!pdfContent[ind + 1].Words.Any(c => char.IsLower(c)) && !pdfContent[ind + 1].Words.Equals(""))
                            {
                                menuContent.AddCategory(line);
                                curCatInd++;
                                menuContent.Menus[curCatInd].AddSubCategory(pdfContent[ind + 1].Words);
                                curSubCatInd = 0;
                                ind++;
                            }
                            else
                            {
                                menuContent.Menus[curCatInd].AddSubCategory(line);
                                curSubCatInd++;
                            }
                        }
                        else
                        {
                            notAddedCount++;
                            failtoadd.Add(line);
                        }
                    }
                    string msg = "Convert complete.";
                    if (falseMenuCount != 0 || notAddedCount != 0)
                    {
                        msg = string.Format("{0} line(s) fail to add menu and {1} line(s) not added\nLines failed to add:\n", falseMenuCount, notAddedCount);
                        foreach (string s in failtoadd)
                            msg += string.Format("- \"{0}\"\n", s);
                    }
                    MessageBox.Show(msg);
                    // menuContent = new MenuClass(pdfContent[0].Words);

                    DataContext = menuContent;
                    cmbCate.ItemsSource = menuContent.Menus;
                    cmbCate.DisplayMemberPath = "Category"; // will tell system to display "Category" field in the collection
                    cmbCate.SelectedIndex = 0;
                    cmbSubCate.ItemsSource = menuContent.Menus[cmbCate.SelectedIndex].Subcategories;
                    cmbSubCate.DisplayMemberPath = "SubCategory";
                    cmbSubCate.SelectedIndex = 0;
                    dgMenu.ItemsSource = menuContent.Menus[0].Subcategories[0].Items;

                    //reset the style
                    System.Windows.Media.FontFamily ff = new System.Windows.Media.FontFamily();
                    txtMenuShopName.FontFamily = ff;
                    txtMenuShopName.FontSize = 12;
                    txtMenuShopName.FontWeight = FontWeights.Regular;
                    txtMenuShopName.FontStyle = FontStyles.Normal;

                    cmbCate.FontFamily = ff;
                    cmbCate.FontSize = 12;
                    cmbCate.FontWeight = FontWeights.Regular;
                    cmbCate.FontStyle = FontStyles.Normal;

                    cmbSubCate.FontFamily = ff;
                    cmbSubCate.FontSize = 12;
                    cmbSubCate.FontWeight = FontWeights.Regular;
                    cmbSubCate.FontStyle = FontStyles.Normal;

                    dgMenu.FontFamily = ff;
                    dgMenu.FontSize = 12;
                    dgMenu.FontWeight = FontWeights.Regular;
                    dgMenu.FontStyle = FontStyles.Normal;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {   // change menu to text 
                InputDialog idLines = new InputDialog("Enter number of lines for each page", "Lines", DataType.aNumber);
                bool? yn = idLines.ShowDialog();
                int pages;
                if (yn == true)
                {
                    pages = ConvertMenuToText(int.Parse(idLines.inputText) - 1);
                }
                else
                    return;
                cmbPages.IsEnabled = true;
                MainContentHolder.Visibility = Visibility.Visible;
                MenuContentHolder.Visibility = Visibility.Hidden;
                textTool.Visibility = Visibility.Visible;
                menuTool.Visibility = Visibility.Hidden;
                btnConvertMenu.Content = "Convert Menu";

                MainContent.Clear();
                currentPageInd = -1;//trigger cmb on change
                cmbPages.Items.Clear();//trigger again
                for (int i = 0; i <= pages; i++)
                    cmbPages.Items.Add(i + 1);
                cmbPages.SelectedIndex = 0;//trigger again
                btnAddPage.IsEnabled = true;
                btnConnect.IsEnabled = false;
                WriteContent(0);
            }
            HideSearch.Visibility = Visibility.Hidden;
            if (filename != null)
            {
                btnService.IsEnabled = true;
                btnWatch.IsEnabled = true;
                btnSave.IsEnabled = true;
            }
            else
            {
                btnService.IsEnabled = false;
                btnWatch.IsEnabled = false;
                btnSave.IsEnabled = false;
            }
            GenHint();
        }

        private void BtnSaveDB_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var fc = new FontConverter();
                //conversion of font to store in database
                System.Drawing.Font f1 = new System.Drawing.Font("Times New Roman", 12);
                var fontAsString = fc.ConvertToInvariantString(f1); // convert to string form
                System.Drawing.Font f2 = (System.Drawing.Font)fc.ConvertFromInvariantString(fontAsString); //convert back to font

                string conn = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection sqlcon = new SqlConnection(conn))
                {
                    sqlcon.Open();
                    int newMenuID, newCateID, newSubCateID; //variables for all items ID
                    //insert new menu
                    using (SqlCommand cmd = new SqlCommand("insert into Menu OUTPUT INSERTED.ShopID values(@name,@addr,@contact,@font,@date,@curre)", sqlcon))
                    {
                        cmd.Parameters.AddWithValue("name", menuContent.ShopName);
                        if (menuContent.ShopAddress == null)
                            cmd.Parameters.AddWithValue("addr", string.Empty);
                        else
                            cmd.Parameters.AddWithValue("addr", menuContent.ShopAddress);
                        if (menuContent.ShopContact == null)
                            cmd.Parameters.AddWithValue("contact", string.Empty);
                        else
                            cmd.Parameters.AddWithValue("contact", menuContent.ShopContact);
                        if (menuContent.DefaultFont != null)
                        {
                            string font = fc.ConvertToInvariantString(menuContent.DefaultFont);
                            cmd.Parameters.AddWithValue("font", font);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("font", string.Empty);
                        }
                        cmd.Parameters.AddWithValue("date", DateTime.Now);
                        cmd.Parameters.AddWithValue("curre", currency);
                        newMenuID = (int)cmd.ExecuteScalar();
                    }
                    //loop through all categories
                    foreach (var cate in menuContent.Menus)
                    {
                        using (SqlCommand catecmd = new SqlCommand("insert into Category OUTPUT INSERTED.CateID values(@name,@font,@shopid)", sqlcon))
                        {
                            catecmd.Parameters.AddWithValue("name", cate.Category);
                            if (cate.DefaultFont != null)
                            {
                                string font = fc.ConvertToInvariantString(cate.DefaultFont);
                                catecmd.Parameters.AddWithValue("font", font);
                            }
                            else
                            {
                                catecmd.Parameters.AddWithValue("font", string.Empty);
                            }
                            catecmd.Parameters.AddWithValue("shopid", newMenuID);
                            newCateID = (int)catecmd.ExecuteScalar(); // get new category ID
                        }
                        foreach (var subcate in cate.Subcategories)
                        {
                            using (SqlCommand subcatecmd = new SqlCommand("insert into SubCategory OUTPUT INSERTED.SubCateID values(@name,@font,@cateid)", sqlcon))
                            {
                                subcatecmd.Parameters.AddWithValue("name", subcate.SubCategory);
                                if (subcate.DefaultFont != null)
                                {
                                    string font = fc.ConvertToInvariantString(subcate.DefaultFont);
                                    subcatecmd.Parameters.AddWithValue("font", font);
                                }
                                else
                                {
                                    subcatecmd.Parameters.AddWithValue("font", string.Empty);
                                }
                                subcatecmd.Parameters.AddWithValue("cateid", newCateID);
                                newSubCateID = (int)subcatecmd.ExecuteScalar();
                            }

                            foreach (var item in subcate.Items)
                            {
                                using (SqlCommand itemcmd = new SqlCommand("insert into Items values(@name,@price,@font,@subcateid)", sqlcon))
                                {
                                    itemcmd.Parameters.AddWithValue("name", item.Item);
                                    itemcmd.Parameters.AddWithValue("price", item.Price);
                                    if (item.DefaultFont != null)
                                    {
                                        string font = fc.ConvertToInvariantString(item.DefaultFont);
                                        itemcmd.Parameters.AddWithValue("font", font);
                                    }
                                    else
                                    {
                                        itemcmd.Parameters.AddWithValue("font", string.Empty);
                                    }
                                    itemcmd.Parameters.AddWithValue("subcateid", newSubCateID);
                                    itemcmd.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                    sqlcon.Close();
                    MessageBox.Show("Completely saved.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnRetrieveDB_Click(object sender, RoutedEventArgs e)
        {
            DateTime selDate;
            menuContent = new MenuClass();
            string formType;
            ComboBoxInput cmbi = new ComboBoxInput();
            cmbi.ShowDialog();
            if (cmbi.DialogResult == true)
            {
                if (cmbi.GetSelectedDate.HasValue)
                    selDate = cmbi.GetSelectedDate.Value;
                else
                    return;
                formType = cmbi.GetFormType;
            }
            else
                return;
            MessageBox.Show(string.Format("Form type : {0}\nLines : {1}\nDate : {2}", formType, cmbi.GetLines, selDate.ToString()));

            string sqlQ = "select m.ShopName,m.Font as ShopFont, m.ShopAddress, m.ShopContact, m.Currency, c.CateName, c.Font as CateFont, " +
                          "s.SubCateName, s.Font as SubCateFont, i.ItemName, i.ItemPrice, i.Font as ItemFont " +
                          "from dbo.Menu as m, dbo.Category as c, dbo.SubCategory as s, dbo.Items as i " +
                          "where m.ShopID = c.ShopID and c.CateID = s.CateID and s.SubCateID = i.SubCateID and m.StoreDate=@val1";

            int curCateInd = -1;
            int curSubCateInd = -1;
            int curItemInd = -1;
            string conn = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection sqlcon = new SqlConnection(conn))
            {
                sqlcon.Open();
                var fc = new FontConverter();
                //Get menu
                using (SqlCommand cmd = new SqlCommand(sqlQ, sqlcon))
                {
                    cmd.Parameters.AddWithValue("val1", selDate);
                    using (SqlDataReader sqlR = cmd.ExecuteReader())
                    {
                        while (sqlR.Read())
                        {
                            //menu level
                            if (menuContent.ShopName == null)
                                menuContent.ShopName = sqlR["ShopName"].ToString();
                            if (menuContent.ShopAddress == null && sqlR["ShopAddress"] != null)
                                menuContent.ShopAddress = sqlR["ShopAddress"].ToString();
                            if (menuContent.ShopContact == null && sqlR["ShopContact"] != null)
                                menuContent.ShopContact = sqlR["ShopContact"].ToString();
                            if (menuContent.DefaultFont == null && sqlR["ShopFont"] != null)
                                menuContent.DefaultFont = (System.Drawing.Font)fc.ConvertFromInvariantString(sqlR["ShopFont"].ToString());
                            if (currency == null)
                                currency = sqlR["Currency"].ToString();


                            //Category level

                            //Comparism fail
                            // LOOK HERE
                            if (!menuContent.ContainsCategory(sqlR["CateName"].ToString()))
                            {
                                curCateInd++;
                                curSubCateInd = -1;
                                curItemInd = -1;
                                menuContent.AddCategory(sqlR["CateName"].ToString());
                                menuContent.Menus[curCateInd].DefaultFont = (System.Drawing.Font)fc.ConvertFromInvariantString(sqlR["CateFont"].ToString());
                            }
                            if (!menuContent.Menus[curCateInd].ContainsSubCate(sqlR["SubCateName"].ToString()))
                            {
                                curSubCateInd++;
                                curItemInd = -1;
                                menuContent.AddSubCategory(curCateInd, sqlR["SubCateName"].ToString());
                                menuContent.Menus[curCateInd].Subcategories[curSubCateInd].DefaultFont = (System.Drawing.Font)fc.ConvertFromInvariantString(sqlR["SubCateFont"].ToString());
                            }
                            if (!menuContent.Menus[curCateInd].Subcategories[curSubCateInd].ContainsItem(sqlR["ItemName"].ToString()))
                            {
                                curItemInd++;
                                menuContent.AddMenuItem(curCateInd, curSubCateInd, sqlR["ItemName"].ToString(), double.Parse(sqlR["ItemPrice"].ToString()));
                                if (menuContent.Menus[curCateInd].Subcategories[curSubCateInd].Items[curItemInd].DefaultFont == null && sqlR["ItemFont"] != null)
                                {
                                    menuContent.Menus[curCateInd].Subcategories[curSubCateInd].Items[curItemInd].DefaultFont = (System.Drawing.Font)fc.ConvertFromInvariantString(sqlR["itemFont"].ToString());
                                }
                            }

                        }
                    }
                }
                sqlcon.Close();
            }

            if (formType.Equals("Text"))
            {
                filename = null;
                int pages = ConvertMenuToText(cmbi.GetLines - 1);

                cmbPages.IsEnabled = true;
                MainContentHolder.Visibility = Visibility.Visible;
                MenuContentHolder.Visibility = Visibility.Hidden;
                textTool.Visibility = Visibility.Visible;
                menuTool.Visibility = Visibility.Hidden;
                btnConvertMenu.Content = "Convert Menu";

                MainContent.Clear();
                currentPageInd = -1;//trigger cmb on change
                cmbPages.Items.Clear();//trigger again
                for (int i = 0; i <= pages; i++)
                    cmbPages.Items.Add(i + 1);
                cmbPages.SelectedIndex = 0;//trigger again
                WriteContent(0);

                cmbPages.IsEnabled = true;
                btnAddPage.IsEnabled = true;
                HideSearch.Visibility = Visibility.Hidden;
                textTool.Visibility = Visibility.Visible;
                menuTool.Visibility = Visibility.Hidden;
                MainContentHolder.Visibility = Visibility.Visible;
                MenuContentHolder.Visibility = Visibility.Hidden;
                btnFind.IsEnabled = true;
                btnSaveAs.IsEnabled = true;
                btnSetStyle.IsEnabled = true;
                btnSetDefaultStyle.IsEnabled = true;
                btnConnect.IsEnabled = true;
                btnConvertMenu.IsEnabled = true;
            }
            else
            {
                DataContext = menuContent;
                cmbCate.ItemsSource = menuContent.Menus;
                cmbCate.DisplayMemberPath = "Category"; // will tell system to display "Category" field in the collection
                cmbCate.SelectedIndex = 0;
                cmbSubCate.ItemsSource = menuContent.Menus[cmbCate.SelectedIndex].Subcategories;
                cmbSubCate.DisplayMemberPath = "SubCategory";
                cmbSubCate.SelectedIndex = 0;
                dgMenu.ItemsSource = menuContent.Menus[0].Subcategories[0].Items;
                textTool.Visibility = Visibility.Hidden;
                menuTool.Visibility = Visibility.Visible;
                HideSearch.Visibility = Visibility.Hidden;
                MainContentHolder.Visibility = Visibility.Hidden;
                MenuContentHolder.Visibility = Visibility.Visible;
                cmbPages.IsEnabled = false;
                btnAddPage.IsEnabled = false;
                btnSaveAs.IsEnabled = true;
                btnConnect.IsEnabled = true;
                btnConvertMenu.IsEnabled = true;

                if (menuContent.DefaultFont != null)
                {
                    txtMenuShopName.FontFamily = new System.Windows.Media.FontFamily(menuContent.DefaultFont.FontFamily.GetName(0).ToString());
                    txtMenuShopName.FontSize = menuContent.DefaultFont.Size;
                    txtMenuShopName.FontWeight = menuContent.DefaultFont.Bold ? FontWeights.Bold : FontWeights.Regular;
                    txtMenuShopName.FontStyle = menuContent.DefaultFont.Italic ? FontStyles.Italic : FontStyles.Normal;
                }

                if (menuContent.Menus[0].DefaultFont != null)
                {
                    cmbCate.FontFamily = new System.Windows.Media.FontFamily(menuContent.Menus[0].DefaultFont.FontFamily.GetName(0).ToString());
                    cmbCate.FontSize = menuContent.Menus[0].DefaultFont.Size;
                    cmbCate.FontWeight = menuContent.Menus[0].DefaultFont.Bold ? FontWeights.Bold : FontWeights.Regular;
                    cmbCate.FontStyle = menuContent.Menus[0].DefaultFont.Italic ? FontStyles.Italic : FontStyles.Normal;
                }
                if (menuContent.Menus[0].Subcategories[0].DefaultFont != null)
                {
                    cmbSubCate.FontFamily = new System.Windows.Media.FontFamily(menuContent.Menus[0].Subcategories[0].DefaultFont.FontFamily.GetName(0).ToString());
                    cmbSubCate.FontSize = menuContent.Menus[0].Subcategories[0].DefaultFont.Size;
                    cmbSubCate.FontWeight = menuContent.Menus[0].Subcategories[0].DefaultFont.Bold ? FontWeights.Bold : FontWeights.Regular;
                    cmbSubCate.FontStyle = menuContent.Menus[0].Subcategories[0].DefaultFont.Italic ? FontStyles.Italic : FontStyles.Normal;
                }
                if (menuContent.Menus[0].Subcategories[0].Items[0].DefaultFont != null)
                {
                    dgMenu.FontFamily = new System.Windows.Media.FontFamily(menuContent.Menus[0].Subcategories[0].Items[0].DefaultFont.FontFamily.GetName(0).ToString());
                    dgMenu.FontSize = menuContent.Menus[0].Subcategories[0].Items[0].DefaultFont.Size;
                    dgMenu.FontWeight = menuContent.Menus[0].Subcategories[0].Items[0].DefaultFont.Bold ? FontWeights.Bold : FontWeights.Regular;
                    dgMenu.FontStyle = menuContent.Menus[0].Subcategories[0].Items[0].DefaultFont.Italic ? FontStyles.Italic : FontStyles.Normal;
                }
            }
            if (filename != null)
            {
                btnService.IsEnabled = true;
                btnWatch.IsEnabled = true;
                btnSave.IsEnabled = true;
            }
            else
            {
                btnService.IsEnabled = false;
                btnWatch.IsEnabled = false;
                btnSave.IsEnabled = false;
            }
            GenHint();
        }

        private void BtnService_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (btnService.Content.Equals("Start Service"))
                {
                    if (sc.GetServiceStatus() == System.ServiceProcess.ServiceControllerStatus.Running)
                    {
                        sc.StopService(10000);
                    }
                    sc.StartService(10000, new string[] { filename.Substring(0, filename.LastIndexOf(@"\")) });
                    btnService.Content = "Stop Service";
                }
                else
                {
                    if (sc.GetServiceStatus() != System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        sc.StopService(10000);
                    }
                    btnService.Content = "Start Service";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnWatch_Click(object sender, RoutedEventArgs e)
        {
            if (btnWatch.Content.Equals("Start Watch"))
            {
                FWC = new FileWatcherClass(filename);
                FWC.LogToDataBase = MessageBox.Show("Store the log in database as well?", "Database", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes;
                FWC.StartWatch();
                btnWatch.Content = "Stop Watch";
            }
            else
            {
                FWC.StopWatch();
                btnWatch.Content = "Start Watch";
            }
        }
        #endregion

        #region private function
        private void StoreData()
        {
            if (currentPageInd >= 0)
            {
                pdfContent = pdfContent.OrderBy(p => p.Page).ThenBy(p => p.Line).ToList(); //linq sort 2 field
                int page = currentPageInd;
                string[] splitContent = MainContent.Text.Split(new char[] { '\n' });
                int pagecountline = -1;
                foreach (ContentClass cc in pdfContent)
                {
                    if (cc.Page == page)
                        pagecountline++;
                }

                for (int contentCount = 0; contentCount < splitContent.Length; contentCount++)
                {
                    bool found = false;
                    for (int pdfdatacount = 0; pdfdatacount < pdfContent.Count && !found; pdfdatacount++)//find the line and page pdfcontent
                    {
                        string sptc = splitContent[contentCount];
                        if (pdfContent[pdfdatacount].Page == page && pdfContent[pdfdatacount].Line == contentCount)
                        {
                            pdfContent[pdfdatacount].Words = splitContent[contentCount];
                            found = true;
                        }
                        if (!found && contentCount > pagecountline)
                        {
                            pdfContent.Add(new ContentClass(page, contentCount, splitContent[contentCount]));
                            found = true;
                        }
                    }
                }
                pdfContent = pdfContent.OrderBy(p => p.Page).ThenBy(p => p.Line).ToList(); //linq sort 2 field
            }
        }

        private void WriteContent(int pageNumber)
        {
            StringBuilder temp = new StringBuilder();
            MainContent.Clear();
            foreach (ContentClass cc in pdfContent)
            {
                if (pageNumber == cc.Page)
                {
                    temp.AppendLine(cc.Words);
                }
            }
            if (temp.Length < 1)
            {
                temp.Append("No content");
            }
            else
            {
                MainContent.AppendText(temp.ToString());
                int wordcount = 0;
                foreach (ContentClass cc in pdfContent)
                {
                    if (pageNumber == cc.Page)
                    {
                        if (cc.Textrange.Count > 0)
                        {// loop through all line to set font
                            foreach (var key in cc.Textrange.Keys.ToList())
                            {
                                MainContent.Select(wordcount + key, cc.Textrange[key]);
                                MainContent.SelectionFont = cc.Style[key];
                            }

                        }
                        wordcount += cc.Words.Length + 1;//1 represent \n
                    }
                }
            }
        }

        private int ConvertMenuToText(int LinesPerPage)
        {
            /*
            Questions:
            1) How identify when to page break?
               Solution : By category?
                          By sub-cateogry?
                          By lines?(best but how? How? count each of it! But how many lines? Too fixed.
                          Let user define how many lines? Maybe?) take this.

            2) How to store?
               Solution : write to page > store > clear > write > store > clear > N times > set to first page(looks easy)
                          direct store to pdfcontent(hard on paging) take this

            3) How about font styling?
               Solution : no fonts on menu and clear all on switching view? DEAL.
             */

            //menuContent;
            pdfContent.Clear();
            int line = 0;//should increase when write line
            int page = 0;
            string content = "";
            pdfContent.Add(new ContentClass(page, line, menuContent.ShopName));
            line++;
            foreach (var Category in menuContent.Menus)//missing shop name
            {
                pdfContent.Add(new ContentClass(page, line, Category.Category));
                line++;
                if (line > LinesPerPage)
                {
                    line = 0;
                    page++;
                    pdfContent.Add(new ContentClass(page, line, menuContent.ShopName));
                    line++;
                }
                foreach (var subCategory in Category.Subcategories)
                {
                    pdfContent.Add(new ContentClass(page, line, subCategory.SubCategory));
                    line++;
                    if (line > LinesPerPage)
                    {
                        line = 0;
                        page++;
                        pdfContent.Add(new ContentClass(page, line, menuContent.ShopName));
                        line++;
                    }
                    foreach (var items in subCategory.Items)
                    {
                        content = items.Item + " " + currency + " " + string.Format("{0:0.00}", items.Price);
                        pdfContent.Add(new ContentClass(page, line, content));
                        line++;
                        if (line > LinesPerPage)
                        {
                            line = 0;
                            page++;
                            pdfContent.Add(new ContentClass(page, line, menuContent.ShopName));
                            line++;
                        }
                    }
                }
            }
            return page;
        }

        private void GenHint()
        {
            hintList.Clear();
            Random rand = new Random();
            List<int> indexes = new List<int>();
                int number;
            if (MainContentHolder.Visibility == Visibility.Visible)
            {
                for (int i = 0; i < TEXTHINTS.Length; i++)
                {
                    do
                    {
                        number = rand.Next(0, TEXTHINTS.Length);
                    } while (indexes.Contains(number));
                    indexes.Add(number);
                }
                foreach (var i in indexes)
                    hintList.Add(TEXTHINTS[i]);
            }
            else
            {
                for (int i = 0; i < MENUHINTS.Length; i++)
                {
                    do
                    {
                        number = rand.Next(0, MENUHINTS.Length );
                    } while (indexes.Contains(number));
                    indexes.Add(number);
                }
                foreach (var i in indexes)
                    hintList.Add(MENUHINTS[i]);
            }
        }
        #endregion

        #region menu content events
        private void CmbCate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                cmbSubCate.ItemsSource = menuContent.Menus[cmbCate.SelectedIndex].Subcategories;
                cmbSubCate.DisplayMemberPath = "SubCategory";
                cmbSubCate.SelectedIndex = 0; //trigger cmbSubCate_SelectionChanged

            }
            catch
            { }
        }

        private void CmbSubCate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                dgMenu.ItemsSource = menuContent.Menus[cmbCate.SelectedIndex].Subcategories[cmbSubCate.SelectedIndex].Items;
            }
            catch { }
        }
        #endregion
        //file system watcher
        // -detect file creation, process and push
        // timer class
    }
}
